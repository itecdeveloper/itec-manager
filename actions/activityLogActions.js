import { getAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import { randomString } from '@util/centerFunc'
import { formatDataActivity } from '@util/formatData'

export const setAllActivity = (allActivity) => {
  return {
    type: 'SET_ALL_ACTIVITY',
    allActivity,
  }
}
export const setAllActivityOriginal = (allActivityOriginal) => {
  return {
    type: 'SET_ALL_ACTIVITY_ORIGINAL',
    allActivityOriginal,
  }
}
export const setAllActivityLoading = (isLoading) => {
  return {
    type: 'SET_ACTIVITY_LOADING',
    isLoading,
  }
}
export const setCourseActivity = (courses) => {
  return {
    type: 'SET_COURSE_ACTIVITY',
    courses,
  }
}
export const setActivityDetailLoading = (isLoading) => {
  return {
    type: 'SET_ACTIVITY_DETAIL_LOADING',
    isLoading,
  }
}

export const initActivityLog = () => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_ALL_ACTIVITY')
  const finalUrl = `${url}?${randomString(5)}`
  dispatch(setAllActivityLoading(true))
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const data = await formatDataActivity(res.message.data)
    await dispatch(setAllActivity(data))
    await dispatch(setAllActivityOriginal(data))
  } else {
  }
  dispatch(setAllActivityLoading(false))
}

export const exportActivity = () => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_ALL_ACTIVITY')
  const finalUrl = `${url}?export=true`
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    window.location = `${res.message.data}`
  } else {
    console.error('error', res)
  }
}

export const getCourseByID = (courseId, id) => async (dispatch, getState) => {
  const courses = getState().activity.courses
  const course = courses[courseId]
  dispatch(setActivityDetailLoading(true))
  if (course) {
  } else {
    const account = getState().account
    const url = getAPIRouter('GET_COURSE_DETAILS')
    const finalUrl = url.replace('param', courseId)
    const header = { Authorization: account.token }
    const res = await getAPIService(finalUrl, header)
    if (res.status) {
      const data = res.message.data
      courses[courseId] = data
      await dispatch(setCourseActivity(courses))
    } else {
      console.error('error', res)
    }
  }
  dispatch(setActivityDetailLoading(false))
}
