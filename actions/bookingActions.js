export const setBookingDetail = (bookingDetail) => {
  return {
    type: 'SET_BOOKING_DETAIL',
    bookingDetail,
  }
}

export const initBookingDetail = (index) => async (dispatch, getState) => {
  const { course } = getState().courses
  dispatch(setBookingDetail(course.LEVEL[index]))
}
