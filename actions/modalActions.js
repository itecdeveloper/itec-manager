export const setOpenModal = (open) => {
  return {
    type: 'SET_OPEN_MODAL',
    open,
  }
}
