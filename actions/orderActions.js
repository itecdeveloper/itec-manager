import { getAPIService, putAPIService, postAPIService, delAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import { formatDataOrders } from '@util/formatData'
import { randomString } from '@util/centerFunc'
import {
  getLevelByCourse,
  setGroupOrders,
  mapCourseData,
  filterLevelById,
  filterTeacherById,
} from '@util/orderFunc'
import {
  filterOrderSuccess,
  filterSessionStatusActivate,
  filterSessionActivate,
  filterDatatableOrders,
} from '@util/filterData'
import { toTimeSteamp, bookAndRescheduleFlow } from '@util/bookingFunc'

export const setAllOrdersLoading = (isLoading) => {
  return {
    type: 'SET_ALL_ORDERS_LOADING',
    isLoading,
  }
}
export const setOrderDetailLoading = (isLoading) => {
  return {
    type: 'SET_ORDER_DETAIL_LOADING',
    isLoading,
  }
}
export const setOriginalOrders = (originalOrders) => {
  return {
    type: 'SET_ORIGINAL_ORDERS',
    originalOrders,
  }
}
export const setAllOrderOriginal = (allOrderOriginal) => {
  return {
    type: 'SET_ALL_ORDER_ORIGINAL',
    allOrderOriginal,
  }
}

export const setAllOrders = (allOrders) => {
  return {
    type: 'SET_ALL_ORDERS',
    allOrders,
  }
}

export const setOrderDetail = (orderDetail) => {
  return {
    type: 'SET_ORDER_DETAIL',
    orderDetail,
  }
}

export const setOrderDetailLevels = (orderDetail) => {
  return {
    type: 'SET_ORDER_DETAIL_LEVELS',
    orderDetail,
  }
}

export const setOrderDetailTeachers = (orderDetail) => {
  return {
    type: 'SET_ORDER_DETAIL_TEACHERS',
    orderDetail,
  }
}

export const setCourseDetails = (courseDetails) => {
  return {
    type: 'SET_COURSE_DETAILS',
    courseDetails,
  }
}

export const setTextSearch = (textSearch) => {
  return {
    type: 'SET_TEXT_SEARCH',
    textSearch,
  }
}

export const initOrders = () => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_ORDERS')
  const finalUrl = `${url}?${randomString(6)}`
  dispatch(setAllOrdersLoading(true))
  dispatch(setTextSearch(''))
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const data = res.message.data
    const orders = filterOrderSuccess(data.ORDERS)
    const uniqueCourseId = [...new Set(orders.map((a) => a.DETAIL.COURSE_ID))]
    await Promise.all(uniqueCourseId.map((data) => dispatch(getCourseById(data))))
    const courseData = await Promise.all(
      uniqueCourseId.map((data) => dispatch(getCourseById(data))),
    )
    dispatch(setCourseDetails(courseData))
    await Promise.all(
      orders.map(async (order, key) => {
        const detail = await mapCourseData(
          order.DETAIL.COURSE_ID,
          order.DETAIL.LEVEL_ID,
          order.DETAIL.TEACHER_ID,
          courseData,
        )
        orders[key] = {
          ...order,
          DETAIL: { ...order.DETAIL, ...detail },
        }
      }),
    )
    const ordersData = await formatDataOrders(orders, data.ACCOUNTS)
    await dispatch(updateOrderAfterActions(ordersData))
  } else {
    console.error('error', res)
  }
  dispatch(setAllOrdersLoading(false))
}

export const getCourseById = (courseId) => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_COURSE_DETAIL')
  const urlFinal = url.replace('param', courseId)
  const res = await getAPIService(urlFinal, header)
  if (res.status) {
    const data = res.message.data
    return data
  } else {
    return {}
  }
}

export const getCourseDetailById = (order) => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_COURSE_DETAIL')
  const urlFinal = url.replace('param', order.COURSE_ID)
  const res = await getAPIService(urlFinal, header)
  let result = {}
  if (res.status) {
    const course = res.message.data
    const level = await filterLevelById(course.LEVEL || [], order.LEVEL_ID)
    const teacher = await filterTeacherById(course.ACCOUNTS || [], order.TEACHER_ID)
    result = {
      COURSE_NAME: course.DETAIL.COURSE_NAME,
      CREATE_TICKET_TIME: course.DETAIL.CREATE_TICKET_TIME,
      CREATE_TICKET_TIME_LIMIT: course.DETAIL.CREATE_TICKET_TIME_LIMIT,
      LEVEL: level.length > 0 ? level[0].LEVEL : '-',
      TEACHER: teacher.length > 0 ? `${teacher[0].FISRT_NAME} ${teacher[0].LAST_NAME}` : '-',
    }
    return result
  } else {
    return result
  }
}

export const getCourseDetail = (courseId) => async (dispatch, getState) => {
  const account = getState().account
  const orderDetail = getState().orders.orderDetail
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_COURSE_DETAIL')
  const finalUrl = url.replace('param', courseId)
  dispatch(setOrderDetailLoading(true))
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const course = res.message.data
    const levels = await getLevelByCourse(course, orderDetail.age)
    let levelTeachers = await levels.filter((level) => level.value === orderDetail.LEVEL_ID)
    await dispatch(setOrderDetailLevels(levels))
    if (levelTeachers.length > 0) {
      await dispatch(setOrderDetailTeachers(levelTeachers[0].teachers))
    }
  } else {
    console.error('error', res)
  }
  dispatch(setOrderDetailLoading(false))
}

export const updateOrder = (orderId, data) => async (dispatch, getState) => {
  const account = getState().account
  const { orderDetail, originalOrders, courseDetails } = getState().orders
  const header = {
    Authorization: account.token,
  }
  let body = {}
  if (data.LEVEL_ID && data.LEVEL_ID.value !== orderDetail.levelId) {
    body = { ...body, LEVEL_ID: data.LEVEL_ID.value }
  }
  if (data.TEACHER_ID && data.TEACHER_ID.value !== orderDetail.teacherId) {
    body = { ...body, TEACHER_ID: data.TEACHER_ID.value }
  }
  if (data.STATUS && data.STATUS.value !== orderDetail.status) {
    body = { ...body, STATUS: data.STATUS.value }
  }
  if (data.PAYMENT_DETAIL !== '') {
    body = { ...body, PAYMENT_DETAIL: data.PAYMENT_DETAIL }
  }
  if (data.SESSION_QUANTITY !== orderDetail.sessionQuantity) {
    body = { ...body, SESSION_QUANTITY: data.SESSION_QUANTITY }
  }
  const url = getAPIRouter('UPDATE_ORDER')
  const finalUrl = url.replace('param', orderId)
  dispatch(setAllOrdersLoading(true))
  const res = await putAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    const data = res.message.data
    const detail = await mapCourseData(
      data.COURSE_ID,
      data.LEVEL_ID,
      data.TEACHER_ID,
      courseDetails,
    )
    const newData = { ...data, ...detail }
    const index = originalOrders.findIndex((item) => item.ORDER_ID === data.ORDER_ID)
    if (index > -1) {
      const sessions = await dispatch(
        updateOrderSessions(orderId, originalOrders[index].SESSIONS, data),
      )
      originalOrders[index] = {
        ...originalOrders[index],
        ...newData,
        SESSIONS: sessions,
      }
    }
    await dispatch(updateOrderAfterActions(originalOrders))
  } else {
    console.error('error', res)
  }
  dispatch(setAllOrdersLoading(false))
}

export const createSession = (fromState) => async (dispatch, getState) => {
  const account = getState().account
  const { orderDetail, originalOrders } = getState().orders
  const url = getAPIRouter('ORDER_SESSION')
  const finalUrl = url.replace('param', orderDetail.ORDER_ID)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const SESSION_START_TIME = toTimeSteamp(
    fromState.type.value,
    fromState.date,
    fromState.time_hour.value,
    fromState.time_min.value,
    fromState.session.value,
  )
  const body = {
    SESSION_START_TIME,
  }
  dispatch(setAllOrdersLoading(true))
  if (
    bookAndRescheduleFlow(
      SESSION_START_TIME[0],
      orderDetail.CREATED_AT,
      orderDetail.CREATE_TICKET_TIME,
      orderDetail.CREATE_TICKET_TIME_LIMIT,
    )
  ) {
    const res = await postAPIService(finalUrl, header, JSON.stringify(body))
    if (res.status) {
      const data = res.message.data
      const newSession = await filterSessionStatusActivate(data.SESSIONS)
      const index = originalOrders.findIndex((item) => item.ORDER_ID === data.ORDER[0].ORDER_ID)
      if (index > -1) {
        originalOrders[index] = {
          ...originalOrders[index],
          SESSIONS: newSession,
        }
      }
      await dispatch(updateOrderAfterActions(originalOrders))
    } else {
      console.error('error', res)
    }
  } else {
    dispatch(
      setAlert({
        text: SESSION_START_TIME,
        type: 'error',
      }),
    )
  }
  dispatch(setAllOrdersLoading(false))
}
export const updateOrderSessions = (orderId, sessions, newData) => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  let body = {
    LEVEL_ID: newData.LEVEL_ID,
    TEACHER_ID: newData.TEACHER_ID,
  }
  const url = getAPIRouter('UPDATE_SESSION')
  const paramURL1 = url.replace('param1', orderId)
  sessions.map(async (session, key) => {
    let items = {
      LEVEL_ID: session.LEVEL_ID,
      TEACHER_ID: session.TEACHER_ID,
    }
    if (['ACTIVE', 'WAITING_APPROVE'].includes(session.S_STATUS)) {
      const paramURL2 = paramURL1.replace('param2', session.SESSION_ID)
      const res = await putAPIService(paramURL2, header, JSON.stringify(body))
      if (res.status) {
        const data = res.message.data
        items = {
          LEVEL_ID: data.LEVEL_ID,
          TEACHER_ID: data.TEACHER_ID,
        }
      } else {
        console.error('error', res)
      }
    }
    sessions[key] = {
      ...sessions[key],
      ...items,
    }
  })
  return sessions
}
export const updateSession = (orderId, sessionId, status, type) => async (dispatch, getState) => {
  const account = getState().account
  const { originalOrders } = getState().orders
  const header = { Authorization: account.token }
  let body = {}
  if (type === 'reschedule') {
    body = { STATUS_NEW_START_TIME: status }
  } else {
    body = { S_STATUS: status }
  }
  const url = getAPIRouter('UPDATE_SESSION')
  const paramURL1 = url.replace('param1', orderId)
  const paramURL2 = paramURL1.replace('param2', sessionId)
  dispatch(setAllOrdersLoading(true))
  const res = await putAPIService(paramURL2, header, JSON.stringify(body))
  if (res.status) {
    const data = res.message.data
    const indexOrder = originalOrders.findIndex((item) => item.ORDER_ID === orderId)
    const indexSession = originalOrders[indexOrder].SESSIONS.findIndex(
      (item) => item.SESSION_ID === sessionId,
    )
    if (indexSession > -1) {
      originalOrders[indexOrder].SESSIONS[indexSession] = {
        ...originalOrders[indexOrder].SESSIONS[indexSession],
        ...data,
      }
      originalOrders[indexOrder].SESSIONS = await filterSessionActivate(
        originalOrders[indexOrder].SESSIONS,
      )
    }
    await dispatch(updateOrderAfterActions(originalOrders))
  } else {
    console.error('error', res)
  }
  dispatch(setAllOrdersLoading(false))
}

export const deleteSession = (orderId, sessionId) => async (dispatch, getState) => {
  const { originalOrders } = getState().orders
  const account = getState().account
  const url = getAPIRouter('ORDER_SESSION')
  const finalUrl = url.replace('param', orderId)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = { SESSIONS: [{ ORDER_ID: orderId, SSID: sessionId }] }
  dispatch(setAllOrdersLoading(true))
  const res = await delAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    const indexOrder = originalOrders.findIndex((item) => item.ORDER_ID === orderId)
    const indexSession = await originalOrders[indexOrder].SESSIONS.findIndex(
      (item) => item.SESSION_ID === sessionId,
    )
    if (indexSession > -1) {
      await originalOrders[indexOrder].SESSIONS.splice(indexSession, 1)
    }
    await dispatch(updateOrderAfterActions(originalOrders))
  } else {
    console.error('error delete session', res)
  }
  dispatch(setAllOrdersLoading(false))
}

export const exportOrders = () => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_ORDERS')
  const finalUrl = `${url}?export=true`
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    window.location = `${res.message.data}`
  } else {
    console.error('error', res)
  }
}

export const updateOrderAfterActions = (orders) => async (dispatch, getState) => {
  const textSearch = getState().orders.textSearch
  const groupOrders = await setGroupOrders(orders)
  await dispatch(setOriginalOrders(orders))
  await dispatch(setAllOrderOriginal(groupOrders))
  if (textSearch !== '') {
    const orders = filterDatatableOrders(groupOrders.orders, textSearch)
    const pendingOrders = filterDatatableOrders(groupOrders.pendingOrders, textSearch)
    await dispatch(setAllOrders({ orders, pendingOrders }))
  } else {
    await dispatch(setAllOrders(groupOrders))
  }
}
