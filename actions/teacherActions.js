import { getAPIService, putAPIService, postAPIService, delAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import {
  formatDataSessionByTeacher,
  compareValues,
  formatMyCourse,
  formatCalendar,
  formatCalendarCourses,
  mergeFormatCalendar,
} from '@util/formatData'
import { toTimeSteampSchedule } from '@util/formatDate'
import moment from 'moment'
import { randomString } from '@util/centerFunc'

export const setAllTeachersLoading = (isLoading) => {
  return {
    type: 'SET_ALL_TEACHERS_LOADING',
    isLoading,
  }
}

export const setBookSlotLoading = (isLoading) => {
  return {
    type: 'SET_BOOK_SLOT_LOADING',
    isLoading,
  }
}

export const setAllTeachers = (allTeachers) => {
  return {
    type: 'SET_ALL_TEACHERS',
    allTeachers,
  }
}
export const setSessionReschedule = (selectTeacher) => {
  return {
    type: 'SET_SESSION_RESCHEDULE',
    selectTeacher,
  }
}
export const setSelectTeacherId = (thisTeacherId) => {
  return {
    type: 'SET_SELECT_TEACHER_ID',
    thisTeacherId,
  }
}
export const setCalendar = (calendar) => {
  return {
    type: 'SET_CALENDAR',
    calendar,
  }
}
export const setSelectCalendar = (selectCalendar) => {
  return {
    type: 'SET_SELECT_CALENDAR',
    selectCalendar,
  }
}
export const setBookSlotStep = (bookSlotStep) => {
  return {
    type: 'SET_BOOKSLOT_STEP',
    bookSlotStep,
  }
}
export const setBookSuccess = (bookSlotSuccess) => {
  return {
    type: 'SET_BOOK_SUCCESS',
    bookSlotSuccess,
  }
}
export const setRemoveBookSlot = (bookSlotRemove) => {
  return {
    type: 'SET_BOOK_REMOVE',
    bookSlotRemove,
  }
}
export const setStudentDetails = (studentDetails) => {
  return {
    type: 'SET_STUDENT_DETAILS',
    studentDetails,
  }
}
export const setViewSession = (viewSession) => {
  return {
    type: 'SET_VIEW_SESSION',
    viewSession,
  }
}
export const initTeachers = () => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_TEACHERS')
  const urlFinal = `${url}&${randomString(5)}`
  dispatch(setAllTeachersLoading(true))
  const res = await getAPIService(urlFinal, header)
  if (res.status) {
    const items = res.message.data.Items
    // await Promise.all(
    //   items.map(async (item, key) => {
    //     let detail = await dispatch(getOrderByTeacherId(item.ACCOUNT_ID))
    //     items[key] = {
    //       ...item,
    //       RESCHEDULE: detail,
    //     }
    //   }),
    // )
    items.sort(compareValues('FISRT_NAME', 'asc'))
    await dispatch(setAllTeachers(items))
  } else {
    console.error('error', res)
  }
  dispatch(setAllTeachersLoading(false))
}
export const getOrderByTeacherId = (id) => async (dispatch, getState) => {
  const account = getState().account
  const teacherDetail = getState().teacher.selectTeacher
  const calendars = getState().teacher.calendars
  const header = {
    Authorization: account.token,
    'Content-Type': 'application/json',
  }
  const url = getAPIRouter('GET_ORDER_BY_TEACHER')
  const finalUrl = url.replace('param', id)
  const res = await getAPIService(finalUrl, header)
  let results = {}
  if (res.status) {
    const items = res.message.data
    const sessions = await formatDataSessionByTeacher(items)
    const uniqueCourseId = [...new Set(items.DETAIL.map((a) => a.COURSE_ID))]
    const uniqueCourseData = await Promise.all(
      uniqueCourseId.map((data) => dispatch(initGetCourseById(data))),
    )
    const fomatData = formatMyCourse(items, uniqueCourseData)
    const calendarCourse = await formatCalendarCourses(fomatData.orderSuccess || [])
    const allCalendar = await mergeFormatCalendar(calendarCourse, calendars)
    Object.keys(allCalendar).map((date) => {
      allCalendar[date].sort(compareValues('START_TIME', 'asc'))
    })
    dispatch(setCalendar(allCalendar))
    dispatch(selectCalendar(moment().format('D MMM YYYY')))
    await Promise.all(
      sessions.map(async (session, key) => {
        let detail = await dispatch(getOrderCourseDetailId(session.courseId, session.levelId))
        sessions[key] = {
          ...session,
          ...detail,
        }
      }),
    )
    results = sessions
  } else {
    console.error('error', res)
  }
  const newData = {
    ...teacherDetail,
    RESCHEDULE: results,
  }
  await dispatch(setSessionReschedule(newData))
}

export const getOrderCourseDetailId = (courseId, levelId) => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_COURSE_DETAIL')
  const finalUrl = url.replace('param', courseId)
  const res = await getAPIService(finalUrl, header)
  let result = {}
  if (res.status) {
    const courseDetail = res.message.data
    result = {
      courseName: courseDetail.DETAIL.COURSE_NAME,
    }
    return result
  } else {
    return result
  }
}

export const updateSessionReschedule = (data) => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('UPDATE_SESSION_RESCHEDULE')
  const paramURL1 = url.replace('param1', data.orderId)
  const finalURL = paramURL1.replace('param2', data.sessionId)
  let body = {
    STATUS_NEW_START_TIME: data.status,
  }
  const res = await putAPIService(finalURL, header, JSON.stringify(body))
  if (res.status) {
    const data = res.message.data

    if (data.STATUS_NEW_START_TIME !== 'req') {
      dispatch(initTeachers())
    }
  } else {
    console.error('error', res)
  }
}
export const initScheduleTeacher = (teacherId) => async (dispatch, getState) => {
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('SCHEDULE_TEACHER_BY_ID')
  const finalUrl = url.replace('param', teacherId)
  const res = await getAPIService(finalUrl, header)
  let calendar = []
  if (res.status) {
    const data = await res.message.data.sort(compareValues('START_TIME', 'asc'))
    calendar = await formatCalendar(data || [])
  } else {
    console.error('error', res)
  }
  await dispatch(setCalendar(calendar))
  await dispatch(selectCalendar(moment().format('D MMM YYYY')))
}
export const selectCalendar = (value) => async (dispatch, getState) => {
  const { calendars } = getState().teacher
  dispatch(setSelectCalendar(calendars[value]))
}
export const createSchedule = (data) => async (dispatch, getState) => {
  const account = getState().account
  const { selectTeacher } = getState().teacher
  const header = { Authorization: account.token }
  const url = getAPIRouter('SCHEDULE_TEACHER')
  const SCHEDULE_DATE_TIME = toTimeSteampSchedule(
    data.TYPE.value,
    data.START_DATE,
    `${data.START_TIME_HOUR.value}:${data.START_TIME_MIN.value}`,
    data.END_DATE,
    `${data.END_TIME_HOUR.value}:${data.END_TIME_MIN.value}`,
  )
  const body = {
    ...SCHEDULE_DATE_TIME,
    TEACHER_ID: selectTeacher.ACCOUNT_ID,
    TITLE: data.TITLE,
  }
  await dispatch(setBookSlotLoading(true))
  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    await dispatch(initScheduleTeacher(selectTeacher.ACCOUNT_ID))
    dispatch(selectCalendar(data.START_DATE))
    let body = {
      TYPE: data.TYPE.value,
      TITLE: data.TITLE,
      START_DATE: data.START_DATE,
      START_TIME: `${data.START_TIME_HOUR.value}:${data.START_TIME_MIN.value}`,
      END_DATE: data.END_DATE,
      END_TIME: `${data.END_TIME_HOUR.value}:${data.END_TIME_MIN.value}`,
    }
    await dispatch(setBookSuccess(body))
    dispatch(setBookSlotStep(2))
  } else {
    console.error('error', res)
  }
  dispatch(setBookSlotLoading(false))
}
export const deleteSchedule = () => async (dispatch, getState) => {
  const account = getState().account
  const data = getState().teacher.bookSlotRemove
  const header = { Authorization: account.token }
  const url = getAPIRouter('SCHEDULE_TEACHER')
  const body = {
    TEACHER_SCHEDULE: [{ TEACHER_ID: data.TEACHER_ID, LAST_LINK: data.LAST_LINK }],
    TEACHER_ID: data.TEACHER_ID,
    TITLE: data.TITLE,
  }
  const res = await delAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    await dispatch(initScheduleTeacher(data.TEACHER_ID))
    dispatch(selectCalendar(data.START_TIME_FORMATE.date))
  } else {
    console.error('error', res)
  }
}
export const initGetCourseById = (id) => async (dispatch, getState) => {
  const url = getAPIRouter('GET_COURSE_DETAILS')
  const finalUrl = url.replace('param', id)

  const res = await getAPIService(finalUrl, {})
  if (res.status) {
    const items = res.message.data
    return items
  } else {
    console.error('error', res)
    return {}
  }
}
export const getStudentCourse = (studentId) => async (dispatch, getState) => {
  const students = getState().teacher.studentDetails
  const account = getState().account
  const header = { Authorization: account.token }
  const url = getAPIRouter('GET_ACCOUNT_BY_ID')
  const finalUrl = url.replace('param', studentId)
  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const items = res.message.data.Items
    const data = [...students, ...items]
    await dispatch(setStudentDetails(data))
  } else {
    console.error('error', res)
  }
}
