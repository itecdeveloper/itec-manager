import { makeStyles } from '@material-ui/core/styles'

const AutocompleteTimeStyles = makeStyles((theme) => ({
  root: {
    borderRight: '1px solid #CCC',
    '& .MuiFormControl-fullWidth': {
      minHeight: '38px',
      maxHeight: '38px',
    },
    '& .MuiFormLabel-root': {
      transform: 'translate(15px, 10px) scale(1)',
      color: '#CCC',
    },
    '& .MuiInputLabel-filled.MuiInputLabel-shrink': {
      display: 'none',
      transform: 'translate(15px, 4px) scale(0.75)',
    },
  },
  rootInput: {
    backgroundColor: '#FFF',
    borderRadius: '5px',
    '&:hover': {
      borderLeft: 'none',
      backgroundColor: '#FFF',
    },
    '&$focused': {
      borderLeft: 'none',
      backgroundColor: '#FFF',
    },
    '&$disabled': {
      backgroundColor: 'transparent',
    },
  },
  disabled: {},
  input: {
    paddingTop: '4px',
  },
  focused: {},
  formControl: {
    marginLeft: '1px',
    minHeight: '36px',
    maxHeight: '36px',
  },
  paper: {
    margin: '8px 0',
    boxShadow: 'inset 0px 0px 4px rgba(0, 0, 0, 0.12)',
  },
  listbox: {
    padding: '8px 8px',
  },
  option: {
    paddingLeft: '8px',
    paddingRight: '8px',
    minHeight: 41,
    '& .MuiListItemText-root': {
      margin: 0,
    },
    '& .MuiTypography-body1': {
      fontWeight: '500',
      color: '#4D4D4D',
    },
  },
  icon: {
    color: theme.palette.primary.main,
  },
  endAdornmentIcon: {
    color: 'rgba(0, 0, 0, 0.5)',
  },
}))

export default AutocompleteTimeStyles
