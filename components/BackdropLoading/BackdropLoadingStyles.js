import { makeStyles } from '@material-ui/core/styles'

const BackdropLoading = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: theme.palette.primary.main,
  },
}))

export default BackdropLoading
