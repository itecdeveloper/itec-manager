import React from 'react'
import Button from '@material-ui/core/Button'
import classNames from 'classnames'
import buttonStyle from './ButtonComponentStyled'

function ButtonComponent({
  color,
  outlined,
  children,
  fullWidth,
  disabled,
  size,
  block,
  link,
  justIcon,
  noneUppercase,
  className,
  ...rest
}) {
  const classes = buttonStyle()
  const btnClasses = classNames({
    [classes.button]: true,
    [classes[size]]: size,
    [classes[color]]: color,
    outlined: outlined,
    [classes.fullWidth]: fullWidth,
    [classes.block]: block,
    [classes.link]: link,
    [classes.noneUppercase]: true,
    disabled: disabled,
    [className]: className,
  })

  return (
    <Button className={btnClasses} {...rest}>
      {children}
    </Button>
  )
}

export default ButtonComponent
