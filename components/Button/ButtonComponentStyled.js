import Colors from '../../lib/styles/Colors'
import { makeStyles } from '@material-ui/core/styles'

const buttonStyle = makeStyles((theme) => ({
  button: {
    minHeight: 'auto',
    minWidth: '163px',
    // backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.main,
    border: 'none',
    borderRadius: '5px',
    position: 'relative',
    padding: '0 24px',
    // margin: '8px',
    fontSize: '18px',
    fontWeight: 'bold',
    fontFamily: `"Inter", sans-serif`,
    textTransform: 'inherit',
    letterSpacing: '0.2px',
    willChange: 'box-shadow, transform',
    transition:
      'box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1)',
    lineHeight: '56px',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    verticalAlign: 'middle',
    touchAction: 'manipulation',
    boxShadow: 'none',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: theme.palette.primary.light,
      // color: theme.palette.secondary.main,
      // boxShadow: 'none',
    },
    '&.outlined': {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.primary.main,
      border: `1px solid ${theme.palette.primary.main}`,
      '&:hover': {
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.primary.main,
      },
      '&.disabled': {
        border: `1px solid ${Colors['disableGrey']}`,
        backgroundColor: theme.palette.secondary.main,
      },
      [theme.breakpoints.down('xs')]: {
        minWidth: '100%',
      },
    },
    '&.disabled': {
      color: Colors['disableGrey'],
      // backgroundColor: Colors['dividerGrey'],
      cursor: 'default',
      pointerEvents: 'none',
    },
    '& .fab,& .fas,& .far,& .fal,& .material-icons': {
      position: 'relative',
      display: 'inline-block',
      top: '0',
      fontSize: '1.45em',
      // margin: '0px 8px 0px -4px',
      verticalAlign: 'middle',
    },
    '& svg': {
      position: 'relative',
      display: 'inline-block',
      top: '0',
      width: '18px',
      height: '18px',
      marginRight: '4px',
      verticalAlign: 'middle',
    },
  },
  primary: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.secondary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.secondary.main,
    },
    '&.outlined': {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.primary.main,
      border: `1px solid ${theme.palette.primary.main}`,
      '&:hover': {
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.primary.main,
      },
      '&.disabled': {
        border: `1px solid ${Colors['disableGrey']}`,
        backgroundColor: theme.palette.secondary.main,
      },
    },
    '&.disabled': {
      color: Colors['disableGrey'],
      backgroundColor: Colors['dividerGrey'],
      cursor: 'default',
      pointerEvents: 'none',
    },
  },
  outlined: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.primary.main,
    border: `1px solid ${theme.palette.primary.main}`,
    '&:hover': {
      backgroundColor: theme.palette.primary.light,
      color: theme.palette.primary.main,
    },
  },
  transparent: {
    '&,&:focus,&:hover,&:visited': {
      color: 'inherit',
      background: 'transparent',
      boxShadow: 'none',
    },
  },
  disabled: {
    opacity: '0.65',
    pointerEvents: 'none',
    backgroundColor: `${Colors['NeutralGrey']}`,
    color: `#8C8C8C`,
  },
  large: {
    padding: '0 24px',
    fontSize: '18px',
    // lineHeight: '48px',
    lineHeight: '64px',
    minWidth: '360px',
    '& .fab,& .fas,& .far,& .fal,& .material-icons': {
      position: 'relative',
      display: 'inline-block',
      top: '0',
      fontSize: '1.45em',
      // margin: '0px 8px 0px -4px',
      verticalAlign: 'middle',
    },
    [theme.breakpoints.down('xs')]: {
      minWidth: '100%',
    },
  },
  medium: {
    padding: '0 24px',
    fontSize: '18px',
    // lineHeight: '48px',
    lineHeight: '56px',
    minWidth: '163px',
    '& .fab,& .fas,& .far,& .fal,& .material-icons': {
      position: 'relative',
      display: 'inline-block',
      top: '0',
      fontSize: '1.45em',
      // margin: '0px 8px 0px -4px',
      verticalAlign: 'middle',
    },
    [theme.breakpoints.down('xs')]: {
      minWidth: '100%',
    },
  },
  small: {
    padding: ' 0 14px',
    fontSize: '14px',
    lineHeight: '37px',
    minWidth: '138px',
    '& .fab,& .fas,& .far,& .fal,& .material-icons': {
      position: 'relative',
      display: 'inline-block',
      top: '0',
      fontSize: '1.45em',
      // margin: '0px 8px 0px 0px',
      verticalAlign: 'middle',
    },
    [theme.breakpoints.down('xs')]: {
      minWidth: '100%',
    },
  },
  fullWidth: {
    width: '100%',
    // margin: 0,
  },
  disabledOutlined: {
    opacity: '0.65',
    pointerEvents: 'none',
    border: `1px solid ${Colors['NeutralGrey']}`,
    color: `#8C8C8C`,
  },
  block: {
    width: '100% !important',
  },
  link: {
    '&,&:hover': {
      backgroundColor: 'transparent',
      color: '#999999',
      boxShadow: 'none',
    },
  },
  noneUppercase: {
    textTransform: 'inherit',
  },
}))

export default buttonStyle
