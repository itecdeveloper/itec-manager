import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import CardStyles from './CardStyled'
import IconButton from '@material-ui/core/IconButton'
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline'

import { getStatic } from '@lib/static'

function CardComponent({ title, detail, image, onClick }) {
  const classes = CardStyles()

  return (
    <Card className={classes.root}>
      <CardMedia className={classes.media} image={image} title="Paella dish" />
      <CardContent classes={{ root: classes.cardContent }}>
        <div className={classes.typographyRoot}>
          <Typography variant="h5" classes={{ h5: classes.typographyH5 }}>
            {title}
          </Typography>
          <IconButton aria-label="delete" className={classes.iconButton} onClick={onClick}>
            <PlayCircleOutlineIcon fontSize="inherit" />
          </IconButton>
        </div>
        <div className={classes.bodyRoot}>
          <Typography variant="body1" component="p" classes={{ body1: classes.typographyBody1 }}>
            {detail}
          </Typography>
        </div>
      </CardContent>
    </Card>
  )
}

CardComponent.defaultProps = {
  title: 'lorem ipsum',
  detail:
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
  image: getStatic('static/images/image_card.png'),
}

export default CardComponent
