import { makeStyles } from '@material-ui/core/styles'

const CardStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 360,
    height: 313,
    boxShadow: '0px 0px 2px rgba(0, 0, 0, 0.14)',
    margin: 'auto',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    maxHeight: '110.5px',
    padding: '16px 18px 23px 24px',

    position: 'relative',
    overflow: 'hidden',
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
    '&:after': {
      content: `''`,
      position: 'absolute',
      bottom: 0,
      right: 0,
      width: '100%',
      height: '22px',
      background: 'linear-gradient(to bottom, rgba(0, 0, 0, 0), #ffffff 59%)',
    },
  },
  typographyRoot: {
    display: 'flex',
    marginBottom: 5,
  },
  typographyH5: {
    color: 'rgba(0, 0, 0, 0.8)',
    lineHeight: '24px',
    flexGrow: 1,
  },
  bodyRoot: {
    paddingRight: 54,
  },
  typographyBody1: {
    color: 'rgba(0, 0, 0, 0.56)',
    lineHeight: '24px',
  },
  iconButton: {
    color: theme.palette.primary.main,
    fontSize: '1.4286rem',
    padding: 0,
  },
}))

export default CardStyles
