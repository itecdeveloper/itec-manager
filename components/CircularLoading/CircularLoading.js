import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

import CircularLoadingStyles from './CircularLoadingStyles'

function CircularLoading({ open }) {
  const classes = CircularLoadingStyles()
  return (
    <div className={classes.loading} open={open}>
      <CircularProgress color="inherit" />
    </div>
  )
}

export default CircularLoading
