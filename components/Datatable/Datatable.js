import React, { Fragment, useState } from 'react'
import classNames from 'classnames'
import Paper from '@material-ui/core/Paper'
import Icon from '@components/Icon'
import moment from 'moment'

import TableContainer from '@material-ui/core/TableContainer'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'

import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import TablePagination from '@material-ui/core/TablePagination'
import Box from '@material-ui/core/Box'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

import datatableStyles from './DatatableStyled'

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index])
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

function EnhancedTableHead({ keyField, headCells, order, orderBy, onRequestSort }) {
  const classes = datatableStyles()
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }
  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            classes={{ head: classes.headTableCell }}
            key={headCell.id}
            align={headCell.alignCell ? headCell.alignCell : 'left'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            {headCell.sort ? (
              <TableSortLabel
                classes={{
                  root: classes.rootTableSortLabel,
                  active: classes.activeTableSortLabel,
                  icon: classes.iconTableSortLabel,
                }}
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                IconComponent={ArrowDropDown}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
        <TableCell classes={{ head: classes.headTableCell }} />
      </TableRow>
    </TableHead>
  )
}

function EnhancedTableBody(props) {
  const { row } = props
  const [open, setOpen] = useState(false)
  const classes = datatableStyles()
  const StatusClasse = classNames({
    [classes.status]: true,
    [classes.statusSuccess]: row.status === 'Successful',
    [classes.statusWaiting]: row.status === 'Waiting',
    [classes.statusCancelled]: row.status === 'Cancelled',
  })
  return (
    <Fragment>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
        >
          {row.orderId}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.courseName}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.level}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.studentId.toUpperCase()}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.teacher}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.paymentMethod}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          <div className={StatusClasse}>{row.status}</div>
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          <IconButton>
            <Icon className={classes.iconExpand}>editBoxOutline</Icon>
          </IconButton>
        </TableCell>
      </TableRow>
    </Fragment>
  )
}

function Datatable({ keyField, columns, data }) {
  const classes = datatableStyles()
  const [order, setOrder] = useState('asc')
  const [orderBy, setOrderBy] = useState('name')
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  return (
    <Fragment>
      <TableContainer component={Paper} className={classes.paper}>
        <Table aria-label="collapsible table">
          <EnhancedTableHead
            keyField={keyField}
            headCells={columns}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
          />
          <TableBody>
            {stableSort(data, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                return <EnhancedTableBody key={row.name} row={row} />
              })}
          </TableBody>
        </Table>
      </TableContainer>
      {data.length > rowsPerPage ? (
        <TablePagination
          rowsPerPageOptions={[]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      ) : null}
    </Fragment>
  )
}
export default Datatable
