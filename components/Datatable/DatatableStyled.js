import { makeStyles } from '@material-ui/core/styles'

const datatableStyle = makeStyles((theme) => ({
  paper: {
    marginTop: '8px',
    boxShadow: 'unset',
    borderRadius: 'unset',
  },
  iconExpand: {
    color: '#CCCCCC',
    fontSize: '1rem',
    cursor: 'pointer',
    overflow: 'unset',
  },
  rootTableCell: {
    fontWeight: 'normal',
    lineHeight: '1.25rem',
    borderBottom: '1px solid rgba(0, 0, 0, 0.06)',
  },
  headTableCell: {
    fontSize: '0.75rem',
    fontWeight: 600,
    lineHeight: '1.125rem',
    color: 'rgba(0, 0, 0, 0.56)',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  bodyTableCell: {
    color: 'rgba(0, 0, 0, 0.6)',
    padding: '4px 16px',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  sizeSmallTableCell: {
    padding: '10px',
    color: '#7A7A7A',
    fontSize: '0.8125rem',
    fontWeight: 'normal',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  smRootTableCell: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.06)',
  },
  smRootTableCellCompleted: {
    borderBottom: '1px solid #F4f4f4',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rootTableSortLabel: {
    '&$activeTableSortLabel': {
      color: 'rgba(0, 0, 0, 0.56)',
    },
  },
  activeTableSortLabel: {},
  iconTableSortLabel: { opacity: 1 },
  status: {
    width: '85px',
    borderRadius: '5px',
    backgroundColor: '#ccc',
    textAlign: 'center',
    fontWeight: 600,
    fontSize: '0.8125rem',
    lineHeight: 1.5,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  statusSuccess: {
    backgroundColor: '#D8F4D2',
    color: '#2D7720',
  },
  statusWaiting: {
    backgroundColor: '#FFEBBA',
    color: '#83610D',
  },
  statusCancelled: {
    backgroundColor: '#FFD8D8',
    color: '#B41B1B',
  },
}))

export default datatableStyle
