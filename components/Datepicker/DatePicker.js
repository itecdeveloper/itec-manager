import React from 'react'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import IconButton from '@material-ui/core/IconButton'

import Icon from '@components/Icon'

import DatePickerStyles from './DatePickerStyles'

function range(from, to) {
  return Array.from({ length: to - from + 1 }, (_v, i) => i + from)
}
const years = range(1975, moment().add(5, 'y').get('year'))
// const years = moment().year()
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

function DatepickerComponent({ customInput, onChange, value, fullWidth, ...rest }) {
  const handleChange = (date) => {
    onChange(moment(date).format('D MMM YYYY'))
  }
  const classes = DatePickerStyles()

  return (
    <div className={classes.root}>
      <DatePicker
        {...rest}
        selected={value && new Date(moment(value, 'D MMM YYYY'))}
        onChange={handleChange}
        customInput={customInput}
        renderCustomHeader={({
          date,
          changeYear,
          changeMonth,
          decreaseMonth,
          increaseMonth,
          prevMonthButtonDisabled,
        }) => {
          return (
            <div className={classes.headerRoot}>
              <div>
                <IconButton className={classes.headerButton} size="small" onClick={decreaseMonth}>
                  <Icon style={{ fontSize: '0.875rem' }}>smArrowBack</Icon>
                </IconButton>
              </div>
              <div style={{ textAlign: 'center' }} className="react-datepicker__current-month">
                <select
                  className={classes.customSelect}
                  value={months[moment(date).month()]}
                  onChange={({ target: { value } }) => changeMonth(months.indexOf(value))}
                >
                  {months.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
                <select
                  className={classes.customSelect}
                  value={moment(date).year()}
                  onChange={({ target: { value } }) => changeYear(value)}
                >
                  {years.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <IconButton className={classes.headerButton} size="small" onClick={increaseMonth}>
                  <Icon style={{ fontSize: '0.875rem' }}>smArrowForward</Icon>
                </IconButton>
              </div>
            </div>
          )
        }}
      />
    </div>
  )
}

export default DatepickerComponent
