import { makeStyles } from '@material-ui/core/styles'

const DatePickerStyles = makeStyles((theme) => ({
  root: {
    '& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle,& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle,& .react-datepicker__year-read-view--down-arrow,& .react-datepicker__month-read-view--down-arrow,& .react-datepicker__month-year-read-view--down-arrow': {
      marginLeft: '-8px',
      position: 'absolute',
    },
    '& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle,& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle,& .react-datepicker__year-read-view--down-arrow,& .react-datepicker__month-read-view--down-arrow,& .react-datepicker__month-year-read-view--down-arrow,& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle::before,& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle::before,& .react-datepicker__year-read-view--down-arrow::before,& .react-datepicker__month-read-view--down-arrow::before,& .react-datepicker__month-year-read-view--down-arrow::before': {
      boxSizing: 'content-box',
      position: 'absolute',
      border: '8px solid transparent',
      height: 0,
      width: '1px',
    },
    '& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle::before,& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle::before,& .react-datepicker__year-read-view--down-arrow::before,& .react-datepicker__month-read-view--down-arrow::before,& .react-datepicker__month-year-read-view--down-arrow::before': {
      content: `''`,
      zIndex: -1,
      borderWidth: '8px',
      left: '-8px',
      borderBottomColor: '#aeaeae',
    },
    '& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle': {
      top: 0,
      marginTop: '-8px',
    },
    '& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle,& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle::before': {
      borderTop: 'none',
      borderBottomColor: '#f0f0f0',
    },
    '& .react-datepicker-popper[data-placement^=bottom] .react-datepicker__triangle::before': {
      top: '-1px',
      borderBottomColor: '#aeaeae',
    },
    '& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle,& .react-datepicker__year-read-view--down-arrow,& .react-datepicker__month-read-view--down-arrow,& .react-datepicker__month-year-read-view--down-arrow': {
      bottom: 0,
      marginBottom: '-8px',
    },
    '& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle,& .react-datepicker__year-read-view--down-arrow,& .react-datepicker__month-read-view--down-arrow,& .react-datepicker__month-year-read-view--down-arrow,& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle::before,& .react-datepicker__year-read-view--down-arrow::before,& .react-datepicker__month-read-view--down-arrow::before,& .react-datepicker__month-year-read-view--down-arrow::before': {
      borderBottom: 'none',
      borderTopColor: '#fff',
    },
    '& .react-datepicker-popper[data-placement^=top] .react-datepicker__triangle::before,& .react-datepicker__year-read-view--down-arrow::before,& .react-datepicker__month-read-view--down-arrow::before,& .react-datepicker__month-year-read-view--down-arrow::before': {
      bottom: '-1px',
      borderTopColor: '#aeaeae',
    },
    '& .react-datepicker-wrapper': {
      display: 'inline-block',
      width: '100%',
      [theme.breakpoints.down('xs')]: {
        width: '100%',
      },
    },
    '& .react-datepicker': {
      fontFamily: `'Inter', sans-serif`,
      fontSize: '0.8rem',
      backgroundColor: '#fff',
      color: '#000',
      border: 'none',
      borderRadius: '10px',
      display: 'inline-block',
      position: 'relative',
      minWidth: 252,
      boxShadow: '0 0px 5px 0px rgba(155, 155, 155, 0.3)',
    },
    '& .react-datepicker--time-only .react-datepicker__triangle': {
      left: '35px',
    },
    '& .react-datepicker--time-only .react-datepicker__time-container': {
      borderLeft: 0,
    },
    '& .react-datepicker--time-only .react-datepicker__time': {
      borderRadius: '0.3rem',
    },
    '& .react-datepicker--time-only .react-datepicker__time-box': {
      borderRadius: '0.3rem',
    },
    '& .react-datepicker__triangle': {
      display: 'none',
      position: 'absolute',
      left: '50px',
    },
    '& .react-datepicker-popper': {
      zIndex: 9,
      right: '0px !important',
      left: 'unset !important',
      top: '-10px !important',
    },
    '& .react-datepicker-popper[data-placement^=bottom]': {
      marginTop: '10px',
    },
    '& .react-datepicker-popper[data-placement^=top]': {
      marginBottom: '10px',
    },
    '& .react-datepicker-popper[data-placement^=right]': {
      marginLeft: '8px',
    },
    '& .react-datepicker-popper[data-placement^=right] .react-datepicker__triangle': {
      left: 'auto',
      right: '42px',
    },
    '& .react-datepicker-popper[data-placement^=left]': {
      marginRight: '8px',
    },
    '& .react-datepicker-popper[data-placement^=left] .react-datepicker__triangle': {
      left: '42px',
      right: 'auto',
    },
    '& .react-datepicker__header': {
      textAlign: 'center',
      backgroundColor: '#fff',
      borderBottom: 'none',
      borderTopLeftRadius: '10px',
      borderTopRightRadius: '10px',
      // paddingTop: '8px',
      position: 'relative',
      margin: '0.4rem',
      marginRight: 16,
      marginLeft: 16,
      marginBottom: '0px',
    },
    '& .react-datepicker__header--time': {
      paddingBottom: '8px',
      paddingLeft: '5px',
      paddingRight: '5px',
    },
    '& .react-datepicker__year-dropdown-container--select,& .react-datepicker__month-dropdown-container--select,& .react-datepicker__month-year-dropdown-container--select,& .react-datepicker__year-dropdown-container--scroll,& .react-datepicker__month-dropdown-container--scroll,& .react-datepicker__month-year-dropdown-container--scroll': {
      display: 'inline-block',
      margin: '0 2px',
    },
    '& .react-datepicker__current-month,& .react-datepicker-time__header,& .react-datepicker-year-header': {
      marginTop: 0,
      color: '#4a4a4a',
      fontWeight: '400',
      fontSize: '16px',
      // textAlign: 'left',
      // marginLeft: '16px',
      // marginBottom: '16px',
    },
    '& .react-datepicker-time__header': {
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
    '& .react-datepicker__navigation': {
      background: 'none',
      lineHeight: '1.7rem',
      textAlign: 'center',
      cursor: 'pointer',
      position: 'absolute',
      top: '10px',
      width: 0,
      padding: 0,
      border: '0.45rem solid transparent',
      zIndex: 1,
      height: '10px',
      width: '10px',
      textIndent: '-999em',
      overflow: 'hidden',
    },
    '& .react-datepicker__navigation--previous': {
      left: '10px',
      borderRightColor: '#ccc',
      '&:hover': {
        borderRightColor: '#b3b3b3',
      },
    },
    '& .react-datepicker__navigation--previous--disabled,& .react-datepicker__navigation--previous--disabled:hover': {
      borderRightColor: '#e6e6e6',
      cursor: 'default',
    },
    '& .react-datepicker__navigation--next': {
      right: '10px',
      borderLeftColor: '#ccc',
    },
    '& .react-datepicker__navigation--next--with-time:not(.react-datepicker__navigation--next--with-today-button)': {
      right: '80px',
    },
    '& .react-datepicker__navigation--next:hover': {
      borderLeftColor: '#b3b3b3',
    },
    '& .react-datepicker__navigation--next--disabled,& .react-datepicker__navigation--next--disabled:hover': {
      borderLeftColor: '#e6e6e6',
      cursor: 'default',
    },
    '& .react-datepicker__navigation--years': {
      position: 'relative',
      top: 0,
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    '& .react-datepicker__navigation--years-previous': {
      top: '4px',
      borderTopColor: '#ccc',
      '&:hover': {
        borderTopColor: '#b3b3b3',
      },
    },
    '& .react-datepicker__navigation--years-upcoming': {
      top: '-4px',
      borderBottomColor: '#ccc',
      '&:hover': {
        borderBottomColor: '#b3b3b3',
      },
    },
    '& .react-datepicker__month-container': {
      float: 'left',
      width: '100%',
    },
    '& .react-datepicker__month': {
      margin: '0.4rem',
      marginBottom: 16,
      textAlign: 'center',
      marginTop: 0,
    },
    '& .react-datepicker__month .react-datepicker__month-text': {
      display: 'inline-block',
      width: '4rem',
      margin: '2px',
    },
    '& .react-datepicker__input-time-container': {
      clear: 'both',
      width: '100%',
      float: 'left',
      margin: '5px 0 10px 15px',
      textAlign: 'left',
    },
    '& .react-datepicker__input-time-container .react-datepicker-time__caption': {
      display: 'inline-block',
    },
    '& .react-datepicker__input-time-container .react-datepicker-time__input-container': {
      display: 'inline-block',
    },
    '& .react-datepicker__input-time-container .react-datepicker-time__input-container .react-datepicker-time__input': {
      display: 'inline-block',
      marginLeft: '10px',
    },
    '& .react-datepicker__input-time-container .react-datepicker-time__input-container .react-datepicker-time__input input': {
      width: '85px',
    },
    '& .react-datepicker__input-time-container .react-datepicker-time__input-container .react-datepicker-time__input input[type=time]::-webkit-inner-spin-button,& .react-datepicker__input-time-container .react-datepicker-time__input-container .react-datepicker-time__input input[type=time]::-webkit-outer-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
    '& .react-datepicker__input-time-container .react-datepicker-time__input-container .react-datepicker-time__input input[type=time]': {
      '-moz-appearance': 'textfield',
    },
    '& .react-datepicker__input-time-container .react-datepicker-time__input-container .react-datepicker-time__delimiter': {
      marginLeft: '5px',
      display: 'inline-block',
    },
    '& .react-datepicker__time-container': {
      float: 'right',
      borderLeft: '1px solid #aeaeae',
      width: '70px',
    },
    '& .react-datepicker__time-container--with-today-button': {
      display: 'inline',
      border: '1px solid #aeaeae',
      borderRadius: '0.3rem',
      position: 'absolute',
      right: '-72px',
      top: 0,
    },
    '& .react-datepicker__time-container .react-datepicker__time': {
      position: 'relative',
      background: 'white',
    },
    '& .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box': {
      width: '70px',
      overflowX: 'hidden',
      margin: '0 auto',
      textAlign: 'center',
    },
    '& .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box ul.react-datepicker__time-list': {
      listStyle: 'none',
      margin: 0,
      height: 'calc(195px + (1.7rem / 2))',
      overflowY: 'scroll',
      paddingRight: '0px',
      paddingLeft: '0px',
      width: '100%',
      boxSizing: 'content-box',
    },
    '& .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box ul.react-datepicker__time-list li.react-datepicker__time-list-item': {
      height: '30px',
      padding: '5px 10px',
      '&:hover': {
        cursor: 'pointer',
        backgroundColor: '#f0f0f0',
      },
    },
    '& .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box ul.react-datepicker__time-list li.react-datepicker__time-list-item--selected': {
      backgroundColor: '#216ba5',
      color: 'white',
      fontWeight: 'bold',
      '&:hover': {
        backgroundColor: '#216ba5',
      },
    },
    '& .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box ul.react-datepicker__time-list li.react-datepicker__time-list-item--disabled': {
      color: '#ccc',
      '&:hover': {
        cursor: 'default',
        backgroundColor: 'transparent',
      },
    },
    '& .react-datepicker__week-number': {
      color: '#ccc',
      display: 'inline-block',
      width: '1.7rem',
      lineHeight: '1.7rem',
      textAlign: 'center',
      margin: '0.166rem',
    },
    '& .react-datepicker__week-number.react-datepicker__week-number--clickable': {
      cursor: 'pointer',
      '&:hover': {
        borderRadius: '0.3rem',
        backgroundColor: '#f0f0f0',
      },
    },
    '& .react-datepicker__day-names,& .react-datepicker__week': {
      whiteSpace: 'nowrap',
    },
    '& .react-datepicker__day-name,& .react-datepicker__day,& .react-datepicker__time-name': {
      // color: '#000',
      fontSize: 13,
      fontWeight: 400,
      display: 'inline-block',
      width: '1.7rem',
      lineHeight: '1.7rem',
      textAlign: 'center',
      margin: '0.166rem',
    },
    '& .react-datepicker__day-name': {
      color: '#9b9b9b',
    },
    '& .react-datepicker__day': {
      color: '#000',
    },
    '& .react-datepicker__month--selected,& .react-datepicker__month--in-selecting-range,& .react-datepicker__month--in-range': {
      borderRadius: '0.3rem',
      backgroundColor: '#216ba5',
      color: '#fff',
    },
    '& .react-datepicker__month--selected:hover,& .react-datepicker__month--in-selecting-range:hover,& .react-datepicker__month--in-range:hover': {
      backgroundColor: '#1d5d90',
    },
    '& .react-datepicker__month--disabled': {
      color: '#ccc',
      pointerEvents: 'none',
      '&:hover': {
        cursor: 'default',
        backgroundColor: 'transparent',
      },
    },
    '& .react-datepicker__day,& .react-datepicker__month-text': {
      cursor: 'pointer',
    },
    '& .react-datepicker__day:hover,& .react-datepicker__month-text:hover': {
      borderRadius: '14px',
      backgroundColor: '#f0f0f0',
    },
    '& .react-datepicker__day--today,& .react-datepicker__month-text--today': {
      fontWeight: 'bold',
      color: '#ED1B24',
    },
    '& .react-datepicker__day--highlighted,& .react-datepicker__month-text--highlighted': {
      borderRadius: '0.3rem',
      backgroundColor: '#3dcc4a',
      color: '#fff',
    },
    '& .react-datepicker__day--highlighted:hover,& .react-datepicker__month-text--highlighted:hover': {
      backgroundColor: '#32be3f',
    },
    '& .react-datepicker__day--highlighted-custom-1,& .react-datepicker__month-text--highlighted-custom-1': {
      color: 'magenta',
    },
    '& .react-datepicker__day--highlighted-custom-2,& .react-datepicker__month-text--highlighted-custom-2': {
      color: 'green',
    },
    '& .react-datepicker__day--selected,& .react-datepicker__day--in-selecting-range,& .react-datepicker__day--in-range,& .react-datepicker__month-text--selected,& .react-datepicker__month-text--in-selecting-range,& .react-datepicker__month-text--in-range': {
      borderRadius: '14px',
      backgroundColor: '#ED1B24',
      color: '#fff',
    },
    '& .react-datepicker__day--selected:hover,& .react-datepicker__day--in-selecting-range:hover,& .react-datepicker__day--in-range:hover,& .react-datepicker__month-text--selected:hover,& .react-datepicker__month-text--in-selecting-range:hover,& .react-datepicker__month-text--in-range:hover': {
      backgroundColor: '#ED1B24',
    },
    '& .react-datepicker__day--keyboard-selected,& .react-datepicker__month-text--keyboard-selected': {
      borderRadius: '14px',
      backgroundColor: '#ED1B24',
      color: '#fff!important',
    },
    '& .react-datepicker__day--keyboard-selected:hover,& .react-datepicker__month-text--keyboard-selected:hover': {
      backgroundColor: '#ED1B24',
    },
    '& .react-datepicker__day--in-selecting-range,& .react-datepicker__month-text--in-selecting-range': {
      backgroundColor: 'rgba(33, 107, 165, 0.5)',
    },
    '& .react-datepicker__month--selecting-range .react-datepicker__day--in-range,& .react-datepicker__month--selecting-range .react-datepicker__month-text--in-range': {
      backgroundColor: '#f0f0f0',
      color: '#000',
    },
    '& .react-datepicker__day--disabled,& .react-datepicker__month-text--disabled': {
      cursor: 'default',
      color: '#ccc',
    },
    '& .react-datepicker__day--disabled:hover,& .react-datepicker__month-text--disabled:hover': {
      backgroundColor: 'transparent',
    },
    '& .react-datepicker__month-text.react-datepicker__month--selected:hover,& .react-datepicker__month-text.react-datepicker__month--in-range:hover': {
      backgroundColor: '#216ba5',
    },
    '& .react-datepicker__month-text:hover': {
      backgroundColor: '#f0f0f0',
    },
    '& .react-datepicker__input-container': {
      position: 'relative',
      display: 'inline-block',
      width: '100%',
      [theme.breakpoints.down('xs')]: {
        width: '100%',
      },
    },
    '& .react-datepicker__year-read-view,& .react-datepicker__month-read-view,& .react-datepicker__month-year-read-view': {
      border: '1px solid transparent',
      borderRadius: '0.3rem',
    },
    '& .react-datepicker__year-read-view:hover,& .react-datepicker__month-read-view:hover,& .react-datepicker__month-year-read-view:hover': {
      cursor: 'pointer',
    },
    '& .react-datepicker__year-read-view:hover .react-datepicker__year-read-view--down-arrow,& .react-datepicker__year-read-view:hover .react-datepicker__month-read-view--down-arrow,& .react-datepicker__month-read-view:hover .react-datepicker__year-read-view--down-arrow,& .react-datepicker__month-read-view:hover .react-datepicker__month-read-view--down-arrow,& .react-datepicker__month-year-read-view:hover .react-datepicker__year-read-view--down-arrow,& .react-datepicker__month-year-read-view:hover .react-datepicker__month-read-view--down-arrow': {
      borderTopColor: '#b3b3b3',
    },
    '& .react-datepicker__year-read-view--down-arrow,& .react-datepicker__month-read-view--down-arrow,& .react-datepicker__month-year-read-view--down-arrow': {
      borderTopColor: '#ccc',
      float: 'right',
      marginLeft: '20px',
      top: '8px',
      position: 'relative',
      borderWidth: '0.45rem',
    },
    '& .react-datepicker__year-dropdown,& .react-datepicker__month-dropdown,& .react-datepicker__month-year-dropdown': {
      backgroundColor: '#f0f0f0',
      position: 'absolute',
      width: '50%',
      left: '25%',
      top: '30px',
      zIndex: 1,
      textAlign: 'center',
      borderRadius: '0.3rem',
      border: '1px solid #aeaeae',
    },
    '& .react-datepicker__year-dropdown:hover,& .react-datepicker__month-dropdown:hover,& .react-datepicker__month-year-dropdown:hover': {
      cursor: 'pointer',
    },
    '& .react-datepicker__year-dropdown--scrollable,& .react-datepicker__month-dropdown--scrollable,& .react-datepicker__month-year-dropdown--scrollable': {
      height: '150px',
      overflowY: 'scroll',
    },
    '& .react-datepicker__year-option,& .react-datepicker__month-option,& .react-datepicker__month-year-option': {
      lineHeight: '20px',
      width: '100%',
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    '& .react-datepicker__year-option:first-of-type,& .react-datepicker__month-option:first-of-type,& .react-datepicker__month-year-option:first-of-type': {
      borderTopLeftRadius: '0.3rem',
      borderTopRightRadius: '0.3rem',
    },
    '& .react-datepicker__year-option:last-of-type,& .react-datepicker__month-option:last-of-type,& .react-datepicker__month-year-option:last-of-type': {
      '-webkit-user-select': 'none',
      '-moz-user-select': 'none',
      '-ms-user-select': 'none',
      userSelect: 'none',
      borderBottomLeftRadius: '0.3rem',
      borderBottomRightRadius: '0.3rem',
    },
    '& .react-datepicker__year-option:hover,& .react-datepicker__month-option:hover,& .react-datepicker__month-year-option:hover': {
      backgroundColor: '#ccc',
    },
    '& .react-datepicker__year-option:hover .react-datepicker__navigation--years-upcoming,& .react-datepicker__month-option:hover .react-datepicker__navigation--years-upcoming,& .react-datepicker__month-year-option:hover .react-datepicker__navigation--years-upcoming': {
      borderBottomColor: '#b3b3b3',
    },
    '& .react-datepicker__year-option:hover .react-datepicker__navigation--years-previous,& .react-datepicker__month-option:hover .react-datepicker__navigation--years-previous,& .react-datepicker__month-year-option:hover .react-datepicker__navigation--years-previous': {
      borderTopColor: '#b3b3b3',
    },
    '& .react-datepicker__year-option--selected,& .react-datepicker__month-option--selected,& .react-datepicker__month-year-option--selected': {
      position: 'absolute',
      left: '15px',
    },
    '& .react-datepicker__close-icon': {
      cursor: 'pointer',
      backgroundColor: 'transparent',
      border: 0,
      outline: 0,
      padding: 0,
      position: 'absolute',
      top: '50%',
      right: '7px',
      height: '16px',
      width: '16px',
      margin: '-8px auto 0',
    },
    '& .react-datepicker__close-icon::after': {
      cursor: 'pointer',
      backgroundColor: '#216ba5',
      color: '#fff',
      borderRadius: '50%',
      position: 'absolute',
      top: 0,
      right: 0,
      height: '16px',
      width: '16px',
      padding: '2px',
      fontSize: '12px',
      lineHeight: 1,
      textAlign: 'center',
      content: ' "00d7" ',
    },
    '& .react-datepicker__today-button': {
      background: '#f0f0f0',
      borderTop: '1px solid #aeaeae',
      cursor: 'pointer',
      textAlign: 'center',
      fontWeight: 'bold',
      padding: '5px 0',
      clear: 'left',
    },
    '& .react-datepicker__portal': {
      position: 'fixed',
      width: '100vw',
      height: '100vh',
      backgroundColor: 'rgba(0, 0, 0, 0.8)',
      left: 0,
      top: 0,
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex',
      zIndex: 2147483647,
    },
    '& .react-datepicker__portal .react-datepicker__day-name,& .react-datepicker__portal .react-datepicker__day,& .react-datepicker__portal .react-datepicker__time-name': {
      width: '3rem',
      lineHeight: '3rem',
    },
    '& @media (max-width: 400px), (max-height: 550px)': {
      '&.react-datepicker__portal .react-datepicker__day-name,&.react-datepicker__portal .react-datepicker__day,&.react-datepicker__portal .react-datepicker__time-name': {
        width: '2rem',
        lineHeight: '2rem',
      },
    },
    '& .react-datepicker__portal .react-datepicker__current-month,& .react-datepicker__portal .react-datepicker-time__header': {
      fontSize: '1.44rem',
    },
    '& .react-datepicker__portal .react-datepicker__navigation': {
      border: '0.81rem solid transparent',
    },
    '& .react-datepicker__portal .react-datepicker__navigation--previous': {
      borderRightColor: '#ccc',
    },
    '& .react-datepicker__portal .react-datepicker__navigation--previous:hover': {
      borderRightColor: '#b3b3b3',
    },
    '& .react-datepicker__portal .react-datepicker__navigation--previous--disabled,& .react-datepicker__portal .react-datepicker__navigation--previous--disabled:hover': {
      borderRightColor: '#e6e6e6',
      cursor: 'default',
    },
    '& .react-datepicker__portal .react-datepicker__navigation--next': {
      borderLeftColor: '#ccc',
    },
    '& .react-datepicker__portal .react-datepicker__navigation--next:hover': {
      borderLeftColor: '#b3b3b3',
    },
    '& .react-datepicker__portal .react-datepicker__navigation--next--disabled,& .react-datepicker__portal .react-datepicker__navigation--next--disabled:hover': {
      borderLeftColor: '#e6e6e6',
      cursor: 'default',
    },
    '& .react-datepicker__day--outside-month': {
      color: '#9b9b9b',
    },
  },
  headerRoot: {
    display: 'flex',
    paddingBottom: 8,
    justifyContent: 'center',
    alignItems: 'center',
    '& .react-datepicker__current-month': {
      flexGrow: 1,
      textAlign: 'left',
    },
  },
  headerButton: {
    color: '#9b9b9b',
    padding: 7,
  },
  customSelect: {
    '-webkit-appearance': 'none',
    appearance: 'none',
    border: 'none',
    background: 'none',
    color: 'inherit',
    fontSize: 'inherit',
    fontWeight: 'inherit',
    margin: '0px 5px',
  },
  renderDayContents: {
    // position: 'relative',
    '&:after': {
      content: `'•'`,
      color: theme.palette.primary.main,
      fontSize: '12px',
      position: 'absolute',
      right: '12px',
      bottom: '-11px',
    },
  },
  rootInputBase: {
    fontSize: '14px',
    color: '#4A4A4A',
    lineHeight: '17px',
    letterSpacing: '0.2px',
    border: '1px solid #CCC',
    borderRadius: '4px',
    padding: '22px 8px',
  },
  inputInputBase: {
    height: 'auto',
    padding: 0,
    fontWeight: 500,
    fontSize: '1rem',
    lineHeight: '1.5rem',
  },
  input: {
    fontSize: '1rem',
    fontWeight: 500,
    lineHeight: '1.5rem',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  rootTextField: {
    // padding: '7px 15px',
    // border: '1px solid #CCC',
    // borderRadius: '4px',
  },
  rootLabel: {
    transform: 'translate(15px, 24px) scale(1)',
    '&$focused': {
      color: '#808080',
    },
  },
  rootInput: {
    marginTop: '0!important',
    borderRadius: '4px',
    border: '1px solid #CCC',
    minHeight: '64px',
    maxHeight: '64px',
    padding: '22px 15px 15px 15px',
    borderLeft: '1px solid #CCC',
    '&$focusedInput': {
      borderLeft: '3px solid #ED1B24',
    },
  },
  focusedInput: {},
  shrinkLabel: {
    top: '4px',
    fontSize: '0.75rem',
    color: '#808080',
    transform: 'translate(15px, 6px) scale(0.75)',
  },
  focused: {},
  icon: {
    fontSize: '1.5rem',
    color: '#CCC',
    cursor: 'pointer',
  },
  underline: {
    '&:before': {
      borderBottom: 'none',
      top: 0,
    },
    '&:after': {
      borderBottom: 'none',
    },
    '&:hover': {
      '&:not(.Mui-disabled)': {
        borderBottom: '1px solid #CCC',
        '&:before': {
          borderBottom: 'none',
        },
      },
    },
  },
}))
export default DatePickerStyles
