import React from 'react'
import Dialog from '@material-ui/core/Dialog'

import DialogStyles from './DialogStyles'

function DialogComponent({ open, onClose, children }) {
  const classes = DialogStyles()

  return (
    <Dialog onClose={onClose} aria-labelledby="dialog-title" open={open}>
      {children}
    </Dialog>
  )
}

DialogComponent.defaultProps = {
  children: (
    <div>
      To subscribe to this website, please enter your email address here. We will send updates
      occasionally.
    </div>
  ),
}

export default DialogComponent
