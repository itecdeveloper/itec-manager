import { makeStyles } from '@material-ui/core/styles'

const DialogStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  headerRoot: {
    flex: '0 0 auto',
    margin: 0,
    padding: '24px 24px 16px 24px',
  },
  headerTitle: {},
  bodyRoot: {
    padding: '0 24px 24px 24px',
    flex: '1 1 auto',
    overflowY: 'auto',
    '-webkit-overflow-scrolling': 'touch',
  },
  iconButton: {
    padding: 4,
    top: '18px',
    right: '18px',
    position: 'absolute',
  },
}))

export default DialogStyles
