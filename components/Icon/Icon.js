import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Icon from '@material-ui/core/Icon'
import classNames from 'classnames'

const useStyles = makeStyles((theme) => ({
  root: {
    fontSize: 'inherit',
  },
}))

export default function Icons({ children, className, fontSize, color, style, ...rest }) {
  const classes = useStyles()
  const iconClasses = classNames({
    ['itec-icons']: true,
    [classes.root]: !fontSize,
    [className]: className,
  })

  return (
    <Icon style={{ fontSize: fontSize, color: color, ...style }} className={iconClasses} {...rest}>
      {children}
    </Icon>
  )
}
