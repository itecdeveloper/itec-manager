import React from 'react'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import AssignmentIcon from '@material-ui/icons/Assignment'

import IconRoundedStyles from './IconRoundedStyles'

function IconRounded({ title, icon }) {
  const classes = IconRoundedStyles()

  return (
    <div className={classes.root}>
      <Avatar variant="rounded" className={classes.avatarRounded}>
        {icon ? icon : <AssignmentIcon />}
      </Avatar>
      <Typography className={classes.titleTypography}>{title}</Typography>
    </div>
  )
}

export default IconRounded
