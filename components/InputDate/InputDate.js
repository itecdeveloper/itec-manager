import React from 'react'
import FormControl from '@material-ui/core/FormControl'
import IconButton from '@material-ui/core/IconButton'

import InputAdornment from '@material-ui/core/InputAdornment'

import Datepicker from '@components/Datepicker'
import Input from '@components/Input'
import Icon from '@components/Icon'

import moment from 'moment'

const InputCustom = ({ onClick, value, errors, fullWidth, ...rest }) => {
  return (
    <Input
      placeholder="D MMM YYYY"
      endAdornment={
        <InputAdornment position="end">
          <IconButton style={{ color: '#CCC', padding: 0, padding: 8 }}>
            <Icon>calendar</Icon>
          </IconButton>
        </InputAdornment>
      }
      // error={errors ? true : false}
      // helperText={errors ? errors.message : ' '}
      {...rest}
      onClick={onClick}
      value={value && moment(value).format('D MMM YYYY')}
      fullWidth={fullWidth}
    />
  )
}

function InputDate({ inputRef, name, ...rest }) {
  return (
    <Datepicker name={name} customInput={<InputCustom inputRef={inputRef} {...rest} />} {...rest} />
  )
}

export default InputDate
