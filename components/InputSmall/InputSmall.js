import React, { Fragment, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import FilledInput from '@material-ui/core/FilledInput'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormControl from '@material-ui/core/FormControl'
import ErrorIcon from '@material-ui/icons/Error'
import Tooltip from '@material-ui/core/Tooltip'

import Icon from '@components/Icon'

import classNames from 'classnames'

import InputStyles from './InputSmallStyles'

const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: theme.palette.primary.main,
  },
  tooltip: {
    backgroundColor: theme.palette.primary.main,
    padding: '6px 8px',
    fontSize: 14,
  },
  popper: {
    top: '-4px !important',
  },
  popperArrow: {
    '&[x-placement*="top"] $arrow': {
      left: 'unset!important',
      right: 15,
    },
  },
}))

function BootstrapTooltip(props) {
  const classes = useStylesBootstrap()
  const modifiers = {
    flip: {
      behavior: ['right', 'top-end'],
    },
  }

  return <Tooltip PopperProps={{ modifiers: modifiers }} arrow classes={classes} {...props} />
}

function InputComponent({
  id,
  label,
  error,
  errorText,
  fullWidth,
  outlined,
  disabled,
  endAdornment,
  ...rest
}) {
  const classes = InputStyles()
  const formControlClasses = classNames({
    [classes.formControl]: true,
    [classes.fullWidth]: fullWidth,
  })
  const inputClasses = classNames({
    [classes.root]: true,
    [classes.outlined]: outlined,
  })

  return (
    <BootstrapTooltip
      open={error ? true : false}
      title={
        <Fragment>
          <p style={{ margin: 0 }}>{errorText}</p>
        </Fragment>
      }
      placement="right"
      arrow
    >
      <FormControl className={formControlClasses} variant="filled" fullWidth={fullWidth}>
        <InputLabel
          htmlFor={id}
          classes={{
            root: classes.label,
            focused: classes.labelFocused,
            shrink: classes.shrink,
            disabled: classes.disabled,
          }}
          disabled={disabled}
        >
          {label}
        </InputLabel>
        <FilledInput
          id={id}
          classes={{
            root: inputClasses,
            adornedEnd: classes.adornedEnd,
            focused: classes.focused,
            disabled: classes.disabled,
          }}
          disableUnderline
          fullWidth
          disabled={disabled}
          endAdornment={
            error ? (
              <InputAdornment position="end" className={classes.inputAdornment}>
                <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 8 }}>alert</Icon>
              </InputAdornment>
            ) : (
              endAdornment
            )
          }
          inputProps={{ min: '1' }}
          {...rest}
        />
      </FormControl>
    </BootstrapTooltip>
  )
}

InputComponent.defaultProps = {
  errorText: 'Invalid',
}

export default InputComponent
