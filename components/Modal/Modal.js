import React from 'react'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

import Icon from '@components/Icon'

import ModalStyles from './ModalStyles'

function ModalComponent({ open, onClose, children, headerChildren, bgColor }) {
  const classes = ModalStyles({ bgColor })

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.root}
      open={open}
      // onClose={onClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <div className={classes.paper}>
          <div className={classes.headerRoot}>
            <div className={classes.headerTitle}>{headerChildren}</div>
            <IconButton className={classes.iconButton} onClick={onClose}>
              <Icon className={classes.icon}>close</Icon>
            </IconButton>
          </div>
          <div className={classes.bodyRoot}>{children}</div>
        </div>
      </Fade>
    </Modal>
  )
}

ModalComponent.defaultProps = {
  children: (
    <div>
      To subscribe to this website, please enter your email address here. We will send updates
      occasionally.
    </div>
  ),
}

export default ModalComponent
