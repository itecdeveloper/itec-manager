import { makeStyles } from '@material-ui/core/styles'

const ModalStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: ({ bgColor }) => (bgColor ? bgColor : theme.palette.background.paper),
    boxShadow: 'none',
    position: 'relative',
    borderRadius: 4,
    outline: 'none',
    maxWidth: '902px',
    maxHeight: '722px',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: '100%',
      borderRadius: 0,
      maxWidth: '100%',
      maxHeight: '100%',
    },
  },
  headerRoot: {
    flex: '0 0 auto',
    margin: 0,
    padding: '24px 24px 16px 24px',
  },
  headerTitle: {
    paddingRight: 20,
  },
  bodyRoot: {
    padding: '16px 24px 24px 24px',
    flex: '1 1 auto',
    overflowY: 'auto',
    '-webkit-overflow-scrolling': 'touch',
  },
  iconButton: {
    padding: 4,
    top: '18px',
    right: '18px',
    position: 'absolute',
  },
  icon: {
    fontSize: '1.25rem',
    color: '#9B9B9B',
  },
}))

export default ModalStyles
