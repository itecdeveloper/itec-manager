import React from 'react'
import classNames from 'classnames'
import SearchStyles from './SearchStyled'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import Icon from '@components/Icon'

function SearchComponent({ className, width, ...rest }) {
  const classes = SearchStyles()
  const InputBaseClasses = classNames({
    [classes.input]: true,
    [className]: className,
  })

  return (
    <Paper className={classNames(classes.root, classes[width])} elevation={0}>
      <InputBase
        classes={{ input: classes.inputBaseCustom }}
        className={InputBaseClasses}
        {...rest}
      />
      <Icon className={classes.icon}>search</Icon>
    </Paper>
  )
}

export default SearchComponent
