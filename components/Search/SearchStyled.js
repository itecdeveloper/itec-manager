import { makeStyles } from '@material-ui/core/styles'

const SearchStyles = makeStyles((theme) => ({
  root: {
    padding: '12px 16px',
    display: 'flex',
    alignItems: 'center',
    width: 445,
    border: '1px solid #CCCCCC',
    borderRadius: 5,
    boxSizing: 'border-box',
    [theme.breakpoints.only('md')]: {
      width: '-webkit-fill-available',
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  input: {
    marginRight: 8,
    flex: 1,
    color: '#4a4a4a',
    fontFamily: 'Inter',
    fontSize: '0.875rem!important',
    lineHeight: '1.25rem',
    fontWeight: 300,
    cursor: 'auto',
    padding: 0,
    height: '24px',
  },
  icon: {
    fontSize: '1rem',
    color: '#9B9B9B',
  },
  sm: {
    width: '100%',
  },
  xs: {
    width: '100%',
  },
  inputBaseCustom: {
    padding: '0!important',
    height: 'auto!important',
  },
}))

export default SearchStyles
