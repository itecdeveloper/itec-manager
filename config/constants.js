export const PAGE_LANGUAGE = {
  TH: {},
  EN: {},
}

export const getAdminUser = (email) => {
  if (email.indexOf('@ticketmelon.com') > 0) return true
  else return false
}

export const getUserAccessPromoCode = (email) => {
  if (
    email === 'por@riseaccel.com' ||
    email === 'rada@wonderfruit.co' ||
    email === 'jon.lor@gmail.com' ||
    email === 'wan@wonderfruit.co' ||
    email === 'larinda@jaithepfestival.com' ||
    email === 'marketing@fungjai.com' ||
    email === 'karmakliq@gmail.com' ||
    email === 'alisa@wonderfruitfestival.com' ||
    email === 'p@homeaway-agency.com'
  )
    return true
  else return false
}

export const AUTH_COOKIE_NAME = 'itec-manager-token'
export const AUTH_COOKIE_MAX_AGE = 60 * 60 * 24 // 24 hour
export const AUTH_COOKIE_ONE_YEAR_AGE = 24 * 60 * 60 * 365 // 1 year
