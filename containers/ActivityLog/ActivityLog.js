import React, { useEffect, useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'

import * as Actions from '@actions/activityLogActions'

import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Button from '@components/Button'
import Icon from '@components/Icon'
import Search from '@components/Search'
import { filterDatatableActivityLog } from '@util/filterData'

import Datatable from './Datatable'
import ActivityLogStyles from './ActivityLogStyled'

function ActivityLog({ actions, activity, isLoading }) {
  const { allActivity, allActivityOriginal } = activity
  const classes = ActivityLogStyles()
  const [textSearch, setTextSearch] = useState('')
  const [page, setPage] = useState(1)
  const [expanded, setExpanded] = useState({})
  useEffect(() => {
    actions.initActivityLog()
  }, [])
  const onChangeSearch = (event) => {
    const text = event.target.value
    setTextSearch(text)
    setPage(1)
    setExpanded({})
    const newData = filterDatatableActivityLog(allActivityOriginal, text)

    actions.setAllActivity(newData)
  }
  const handleChangePage = (newPage) => {
    setPage(newPage)
    setExpanded({})
  }
  const handleExpand = (id) => {
    setExpanded({
      ...expanded,
      [id]: !expanded[id],
    })
  }
  const columns = [
    { id: 'CREATED_AT', sort: true, label: 'Date', width: '158' },
    { id: 'ACTIVITY_TYPE', sort: true, label: 'Activity Type', width: '108' },
    { id: 'ORDER_ID', sort: true, label: 'Order ID', width: '165' },
    { id: 'ACCOUNT_TYPE', sort: true, label: 'By' },
    { id: 'NAME', sort: true, label: 'Name' },
  ]
  return (
    <Container maxWidth="md">
      <Grid container alignItems="center">
        <Grid item xs={12} lg={3}>
          <Typography variant="h4" className={classes.typographyH4}>
            Activity Log
          </Typography>
        </Grid>
        <Grid item xs={12} lg={9}>
          <Grid container alignItems="center" justify="flex-end">
            <Grid item xs={12} sm={8} lg={'auto'}>
              <Search
                value={textSearch}
                onChange={onChangeSearch}
                placeholder="Search Order ID, Name, etc"
              />
            </Grid>
            <Grid item xs={12} sm={4} lg={'auto'}>
              <Button
                color="primary"
                size="small"
                className={classes.buttonExport}
                onClick={() => actions.exportActivity()}
              >
                Export List <Icon className={classes.icon}>export</Icon>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <div className={classes.rootDatatable}>
        <Datatable
          keyField="tableStudents"
          columns={columns}
          data={allActivity}
          expandRow="right"
          isLoading={isLoading}
          page={page}
          onChangePage={handleChangePage}
          expanded={expanded}
          onClickExpand={handleExpand}
        />
      </div>
    </Container>
  )
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading.allActivityLoading,
    activity: state.activity,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(ActivityLog)
