import React, { Fragment, useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import classNames from 'classnames'
import moment from 'moment'

import * as Actions from '@actions/activityLogActions'

import Paper from '@material-ui/core/Paper'
import TableContainer from '@material-ui/core/TableContainer'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import { Pagination, PaginationItem } from '@material-ui/lab'
import withWidth from '@material-ui/core/withWidth'
import CircularLoading from '@components/CircularLoading'
import { displayShortID } from '@util/centerFunc'

import Icon from '@components/Icon'
import datatableStyles from './DatatableStyled'

import Details from '../Details'

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index])
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

function EnhancedTableHead({ headCells, order, orderBy, onRequestSort, width }) {
  const classes = datatableStyles()
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }
  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            classes={{ head: classes.headTableCell }}
            key={headCell.id}
            align={headCell.alignCell ? headCell.alignCell : 'left'}
            sortDirection={orderBy === headCell.id ? order : false}
            style={{ width: headCell.width ? `${headCell.width}px` : 'auto' }}
          >
            {headCell.sort ? (
              <TableSortLabel
                classes={{
                  root: classes.rootTableSortLabel,
                  active: classes.activeTableSortLabel,
                  icon: classes.iconTableSortLabel,
                }}
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                IconComponent={ArrowDropDown}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
        <TableCell classes={{ head: classes.headTableCell }} />
      </TableRow>
    </TableHead>
  )
}

function EnhancedTableBody({
  data,
  order,
  orderBy,
  page,
  rowsPerPage,
  expanded,
  onClickExpand,
  actions,
  isLoadingDetail,
}) {
  const classes = datatableStyles()
  const handleExpand = (id, courseId, type) => {
    onClickExpand(id)
    if (type === 'Edit Order' || type === 'Edit Session') {
      actions.getCourseByID(courseId, id)
    }
  }
  return (
    <Fragment>
      <TableBody>
        {stableSort(data, getComparator(order, orderBy))
          .slice((page - 1) * rowsPerPage, (page - 1) * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <Fragment>
                <TableRow key={index}>
                  <TableCell
                    component="th"
                    scope="row"
                    classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
                  >
                    {moment(row.CREATED_AT).format('DD MMM YYYY, HH:mm')}
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    <span style={{ fontWeight: '600', textTransform: 'capitalize' }}>
                      {row.ACTIVITY_TYPE}
                    </span>
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    <span style={{ fontWeight: '600' }}>{displayShortID(row.ORDER_ID)}</span>
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    <span style={{ fontWeight: '600' }}>{row.ACCOUNT_TYPE}</span>
                  </TableCell>
                  <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
                    {row.NAME}
                  </TableCell>
                  <TableCell
                    classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
                    style={{ width: '25px' }}
                  >
                    <IconButton
                      onClick={() => {
                        handleExpand(row.ACTIVITY_LOG_ID, row.NEW.COURSE_ID, row.ACTIVITY_TYPE)
                      }}
                    >
                      {expanded[row.ACTIVITY_LOG_ID] ? (
                        <Icon className={classes.iconExpand}>arrowDownward</Icon>
                      ) : (
                        <Icon className={classes.iconExpand}>arrowForward</Icon>
                      )}
                    </IconButton>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    className={classes.subTableCell}
                    colSpan={6}
                    style={{ paddingBottom: expanded[row.ACTIVITY_LOG_ID] ? '8px' : 0 }}
                  >
                    <Collapse
                      in={expanded ? expanded[row.ACTIVITY_LOG_ID] : false}
                      timeout="auto"
                      unmountOnExit
                      className={classes.collapse}
                    >
                      <Details row={row} isLoading={isLoadingDetail} />
                    </Collapse>
                  </TableCell>
                </TableRow>
              </Fragment>
            )
          })}
      </TableBody>
    </Fragment>
  )
}

function PaginationItemCustom({ item, pageAll }) {
  const classes = datatableStyles()
  const rootItemClasse = classNames({
    [classes.rootItem]: true,
    [classes.rootLastItem]: pageAll === item.page,
  })
  if (item.type === 'previous' || item.type === 'next') {
    return (
      <PaginationItem
        classes={{ root: classes.rootItemIcon, selected: classes.selectedItem }}
        {...item}
      />
    )
  } else {
    return (
      <PaginationItem
        classes={{
          root: rootItemClasse,
          page: classes.pageItem,
          selected: classes.selectedItem,
          ellipsis: classes.ellipsisItem,
          focusVisible: classes.focusVisible,
        }}
        {...item}
      />
    )
  }
}

function LoadingData() {
  const classes = datatableStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell colSpan={8} className={classes.loadingTableCell}>
          <CircularLoading />
        </TableCell>
      </TableRow>
    </TableBody>
  )
}

function EmptyData() {
  const classes = datatableStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell colSpan={8} className={classes.emptyTableCell}>
          No data to display
        </TableCell>
      </TableRow>
    </TableBody>
  )
}

function Datatable({
  keyField,
  columns,
  data,
  sizePerPage,
  width,
  page,
  onChangePage,
  expanded,
  onClickExpand,
  isLoading,
  isLoadingDetail,
  actions,
}) {
  const classes = datatableStyles()
  const [order, setOrder] = useState('desc')
  const [orderBy, setOrderBy] = useState('CREATED_AT')
  const [rowsPerPage, setRowsPerPage] = useState(sizePerPage)
  const dataAll = data ? data.length : 0
  const pageAll = Math.ceil(dataAll / rowsPerPage)
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleChangePage = (event, newPage) => {
    onChangePage(newPage)
  }

  return (
    <Fragment>
      <TableContainer component={Paper} className={classes.paper}>
        <Table aria-label="collapsible table" id={keyField}>
          <EnhancedTableHead
            headCells={columns}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            width={width}
          />
          {isLoading ? (
            <LoadingData />
          ) : dataAll > 0 ? (
            <EnhancedTableBody
              data={data}
              order={order}
              orderBy={orderBy}
              page={page}
              rowsPerPage={rowsPerPage}
              expanded={expanded}
              onClickExpand={onClickExpand}
              actions={actions}
              isLoadingDetail={isLoadingDetail}
            />
          ) : (
            <EmptyData />
          )}
        </Table>
      </TableContainer>

      {dataAll > rowsPerPage ? (
        <Pagination
          count={pageAll}
          page={page}
          onChange={handleChangePage}
          classes={{ root: classes.rootPagination, ul: classes.ulPagination }}
          renderItem={(item) => <PaginationItemCustom item={item} pageAll={pageAll} />}
        />
      ) : null}
    </Fragment>
  )
}
Datatable.defaultProps = {
  sizePerPage: 10,
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading.allActivityLoading,
    isLoadingDetail: state.isLoading.activityDetailLoading,
    activity: state.activity,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps), withWidth())

export default enhancer(Datatable)
