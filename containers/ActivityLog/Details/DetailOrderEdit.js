import React from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import classNames from 'classnames'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

import Icon from '@components/Icon'

import { showOrderStatus, showOrderPaymentType } from '@util/orderFunc'

import DetailStyles from './DetailStyles'

function mapTeacherName(teacherId, accounts) {
  const teacher = accounts.find((account) => account.ACCOUNT_ID === teacherId)
  return `${teacher?.FISRT_NAME} ${teacher?.LAST_NAME}`
}
function mapLevelName(levelId, levels) {
  const level = levels.find((level) => level.LEVEL_ID === levelId)
  return `${level?.LEVEL}`
}

function DetailOrderEdit({ row, courses }) {
  const classes = DetailStyles()
  return (
    <TableBody>
      {row.OLD.STATUS !== row.NEW.STATUS && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Payment Status</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.STATUS ? (
              <div
                className={classNames({
                  [classes.status]: true,
                  [classes.statusSuccess]: row.OLD.STATUS === 'PAID',
                  [classes.statusWaiting]:
                    row.OLD.STATUS === 'WAITING' || row.OLD.STATUS === 'ORDER_CREATE',
                  [classes.statusCancel]:
                    row.OLD.STATUS === 'CANCEL' ||
                    row.OLD.STATUS === 'REJECT' ||
                    row.OLD.STATUS === 'INACTIVE',
                })}
              >
                {showOrderStatus(row.OLD.STATUS)}
              </div>
            ) : (
              '-'
            )}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.STATUS ? (
              <div
                className={classNames({
                  [classes.status]: true,
                  [classes.statusSuccess]: row.NEW.STATUS === 'PAID',
                  [classes.statusWaiting]:
                    row.NEW.STATUS === 'WAITING' || row.NEW.STATUS === 'ORDER_CREATE',
                  [classes.statusCancel]:
                    row.NEW.STATUS === 'CANCEL' ||
                    row.NEW.STATUS === 'REJECT' ||
                    row.NEW.STATUS === 'INACTIVE',
                })}
              >
                {showOrderStatus(row.NEW.STATUS)}
              </div>
            ) : (
              '-'
            )}
          </TableCell>
        </TableRow>
      )}
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Payment Method</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {showOrderPaymentType(row.OLD.PAYMENT_TYPE)}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          {showOrderPaymentType(row.NEW.PAYMENT_TYPE)}
        </TableCell>
      </TableRow>
      {row.OLD.PAYMENT_DETAIL !== row.NEW.PAYMENT_DETAIL && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Ref no.</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
            style={{ wordBreak: 'break-word' }}
          >
            {row.OLD.PAYMENT_DETAIL}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ wordBreak: 'break-word' }}
          >
            {row.NEW.PAYMENT_DETAIL}
          </TableCell>
        </TableRow>
      )}
      {row.OLD.LEVEL_ID !== row.NEW.LEVEL_ID && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Level</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.LEVEL_ID !== '-'
              ? mapLevelName(
                  row.OLD.LEVEL_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].LEVEL : [],
                )
              : '-'}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.LEVEL_ID !== '-'
              ? mapLevelName(
                  row.NEW.LEVEL_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].LEVEL : [],
                )
              : '-'}
          </TableCell>
        </TableRow>
      )}
      {row.OLD.TEACHER_ID !== row.NEW.TEACHER_ID && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Teacher Name</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.TEACHER_ID !== '-'
              ? mapTeacherName(
                  row.OLD.TEACHER_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].ACCOUNTS : [],
                )
              : '-'}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.TEACHER_ID !== '-'
              ? mapTeacherName(
                  row.NEW.TEACHER_ID,
                  courses[row.NEW.COURSE_ID] ? courses[row.NEW.COURSE_ID].ACCOUNTS : [],
                )
              : '-'}
          </TableCell>
        </TableRow>
      )}
      {row.OLD.SESSION_QUANTITY !== row.NEW.SESSION_QUANTITY && (
        <TableRow>
          <TableCell
            component="th"
            scope="row"
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '145px' }}
          >
            <span className={classes.info}>Session Quantity</span>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '245px' }}
          >
            {row.OLD.SESSION_QUANTITY}
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
            style={{ width: '55px' }}
          >
            <Icon className={classes.icon}>smArrowForward</Icon>
          </TableCell>
          <TableCell
            classes={{
              root: classes.smRootTableCell,
              body: classes.smBodyTableCell,
            }}
          >
            {row.NEW.SESSION_QUANTITY}
          </TableCell>
        </TableRow>
      )}
    </TableBody>
  )
}
const mapStateToProps = (state) => {
  return {
    courses: state.activity.courses,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(DetailOrderEdit)
