import React from 'react'
import classNames from 'classnames'
import moment from 'moment'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

import Icon from '@components/Icon'

import { showStatusReschedule } from '@util/orderFunc'

import DetailStyles from './DetailStyles'

function DetailRescheduleRequest({ row }) {
  const classes = DetailStyles()
  return (
    <TableBody>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Start Time</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {row.OLD.NEW_START_TIME
            ? moment(row.OLD.NEW_START_TIME).format('ddd, MMMM DD • HH:mm')
            : '-'}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          {row.NEW.NEW_START_TIME
            ? moment(row.NEW.NEW_START_TIME).format('ddd, MMMM DD • HH:mm')
            : '-'}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell
          component="th"
          scope="row"
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '145px' }}
        >
          <span className={classes.info}>Request Status</span>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '245px' }}
        >
          {row.OLD.STATUS_NEW_START_TIME ? (
            <div
              className={classNames({
                [classes.status]: true,
                [classes.statusSuccess]: row.OLD.STATUS_NEW_START_TIME === 'approve',
                [classes.statusWaiting]: row.OLD.STATUS_NEW_START_TIME === 'req',
                [classes.statusCancel]:
                  row.OLD.STATUS_NEW_START_TIME === 'reject' ||
                  row.OLD.STATUS_NEW_START_TIME === 'cancel',
                [classes.statusEmpty]: row.OLD.STATUS_NEW_START_TIME === '-',
              })}
            >
              {showStatusReschedule(row.OLD.STATUS_NEW_START_TIME)}
            </div>
          ) : (
            '-'
          )}
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
          style={{ width: '55px' }}
        >
          <Icon className={classes.icon}>smArrowForward</Icon>
        </TableCell>
        <TableCell
          classes={{
            root: classes.smRootTableCell,
            body: classes.smBodyTableCell,
          }}
        >
          {row.NEW.STATUS_NEW_START_TIME ? (
            <div
              className={classNames({
                [classes.status]: true,
                [classes.statusSuccess]: row.NEW.STATUS_NEW_START_TIME === 'approve',
                [classes.statusWaiting]: row.NEW.STATUS_NEW_START_TIME === 'req',
                [classes.statusCancel]:
                  row.NEW.STATUS_NEW_START_TIME === 'reject' ||
                  row.NEW.STATUS_NEW_START_TIME === 'cancel',
                [classes.statusEmpty]: row.NEW.STATUS_NEW_START_TIME === '-',
              })}
            >
              {showStatusReschedule(row.NEW.STATUS_NEW_START_TIME)}
            </div>
          ) : (
            '-'
          )}
        </TableCell>
      </TableRow>
    </TableBody>
  )
}
export default DetailRescheduleRequest
