import { makeStyles } from '@material-ui/core/styles'

const DetailStyles = makeStyles((theme) => ({
  smRootTableCell: {
    borderBottom: 'unset',
  },
  smBodyTableCell: {
    padding: '4px 10px',
    fontSize: '0.8125rem',
    fontWeight: 'normal',
    lineHeight: 1.5,
    color: 'rgba(0, 0, 0, 0.8)',
    fontWeight: 'normal',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  info: { color: '#808080' },
  icon: {
    color: 'rgba(0, 0, 0, 0.6)',
  },
  status: {
    width: '85px',
    fontSize: '0.8125rem',
    fontWeight: 600,
    lineHeight: 1.5,
    textAlign: 'center',
    backgroundColor: '#CCCCCC',
    borderRadius: '5px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  statusSuccess: {
    color: '#2D7720',
    backgroundColor: '#D8F4D2',
  },
  statusWaiting: {
    color: '#83610D',
    backgroundColor: '#FFEBBA',
  },
  statusCancel: {
    color: '#B41B1B',
    backgroundColor: '#FFD8D8',
  },
  statusEmpty: {
    color: 'rgba(0, 0, 0, 0.7)',
    backgroundColor: 'transparent',
    textAlign: 'left',
  },
}))

export default DetailStyles
