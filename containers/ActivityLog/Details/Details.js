import React from 'react'
import Box from '@material-ui/core/Box'
import Table from '@material-ui/core/Table'

import CircularLoading from '@components/CircularLoading'

import DetailOrderCreate from './DetailOrderCreate'
import DetailOrderCancel from './DetailOrderCancel'
import DetailOrderEdit from './DetailOrderEdit'
import DetailRescheduleRequest from './DetailRescheduleRequest'
import DetailRescheduleApprove from './DetailRescheduleApprove'
import DetailRescheduleCancel from './DetailRescheduleCancel'
import DetailSessionReject from './DetailSessionReject'
import DetailSessionApprove from './DetailSessionApprove'
import DetailSessionComplete from './DetailSessionComplete'
import DetailSessionIncomplete from './DetailSessionIncomplete'
import DetailSessionCreate from './DetailSessionCreate'
import DetailSessionEdit from './DetailSessionEdit'

function Details({ row, isLoading }) {
  return (
    <Box margin={1}>
      {isLoading ? (
        <div style={{ textAlign: 'center' }}>
          <CircularLoading />
        </div>
      ) : (
        <Table size="small" aria-label="purchases">
          {row.ACTIVITY_TYPE === 'Create Order' && <DetailOrderCreate row={row} />}
          {row.ACTIVITY_TYPE === 'Cancel Order' && <DetailOrderCancel row={row} />}
          {row.ACTIVITY_TYPE === 'Reject Order' && <DetailOrderCancel row={row} />}
          {row.ACTIVITY_TYPE === 'Edit Order' && <DetailOrderEdit row={row} />}
          {row.ACTIVITY_TYPE === 'Request Reschedule' && <DetailRescheduleRequest row={row} />}
          {row.ACTIVITY_TYPE === 'Approve Reschedule' && <DetailRescheduleApprove row={row} />}
          {row.ACTIVITY_TYPE === 'Cancel Reschedule' && <DetailRescheduleCancel row={row} />}
          {row.ACTIVITY_TYPE === 'Deny Reschedule' && <DetailRescheduleCancel row={row} />}
          {row.ACTIVITY_TYPE === 'Deny Session' && <DetailSessionReject row={row} />}
          {row.ACTIVITY_TYPE === 'Approve Session' && <DetailSessionApprove row={row} />}
          {row.ACTIVITY_TYPE === 'Complete Session' && <DetailSessionComplete row={row} />}
          {row.ACTIVITY_TYPE === 'Incomplete Session' && <DetailSessionIncomplete row={row} />}
          {row.ACTIVITY_TYPE === 'Create Session' && <DetailSessionCreate row={row} />}
          {row.ACTIVITY_TYPE === 'Edit Session' && <DetailSessionEdit row={row} />}
        </Table>
      )}
    </Box>
  )
}
export default Details
