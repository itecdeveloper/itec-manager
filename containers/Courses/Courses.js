import React from 'react'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Card from '@components/Card'
import TextUnderline from '@components/TextUnderline'
import CoursesStyles from './CoursesStyled'

function Courses({ items }) {
  const classes = CoursesStyles()

  return (
    <div className={classes.root}>
      <Container maxWidth="lg">
        <TextUnderline>Our Courses</TextUnderline>
        <div style={{ backgroundColor: '#cfe8fc' }}>
          <Grid container spacing={1}>
            {items.map((item, key) => (
              <Grid key={key} item xs={4}>
                <Card title={item.name} detail={item.email} />
              </Grid>
            ))}
          </Grid>
        </div>
      </Container>
    </div>
  )
}

export default Courses
