import React from 'react'
import { getStatic } from '@lib/static'
import IconButton from '@material-ui/core/IconButton'
import Icon from '@components/Icon'
import FooterStyles from './FooterStyled'

function Footer() {
  const classes = FooterStyles()

  return (
    <footer className={classes.root}>
      <div className={classes.footerGroup}>
        <div>
          <img
            src={`${getStatic('static/images/ECC_Logo.png')}`}
            alt="image"
            style={{ width: 92 }}
          />
        </div>
        <div className={classes.copyrightRoot}>
          <img
            src={`${getStatic('static/images/copyright.png')}`}
            alt="image"
            style={{ width: 106, height: 13 }}
          />
          <img
            src={`${getStatic('static/images/link.png')}`}
            alt="image"
            style={{ width: 112, height: 14 }}
          />
        </div>
        <div>
          <IconButton aria-label="delete" className={classes.iconButton}>
            <Icon>facebook</Icon>
          </IconButton>
          <IconButton aria-label="delete" className={classes.iconButton}>
            <Icon>twitter</Icon>
          </IconButton>
          <IconButton aria-label="delete" className={classes.iconButton}>
            <Icon>youtube</Icon>
          </IconButton>
        </div>
      </div>
    </footer>
  )
}

export default Footer
