import { makeStyles } from '@material-ui/core/styles'

const FooterStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '217px',
    display: 'block',
    fontFamily: 'Inter',
    fontSize: 14,
    fontWeight: '300',
    letterSpacing: '0.3px',
    lineHeight: '24px',
    backgroundColor: theme.palette.secondary.main,
    borderBottom: `8px solid ${theme.palette.primary.main}`,
    borderTop: `2px solid #F2F2F2`,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  copyrightRoot: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  footerGroup: {
    width: 234,
    textAlign: 'center',
  },
  iconButton: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.secondary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.secondary.main,
    },
  },
}))

export default FooterStyles
