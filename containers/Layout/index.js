import React from 'react'

function MainLayout({ children }) {
  return (
    <div className="main-container">
      <main>{children}</main>
    </div>
  )
}

export default MainLayout
