import React, { useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import Grid from '@material-ui/core/Grid'

import * as Actions from '@actions/teacherActions'

import Orders from '@containers/Orders'
import Teachers from '@containers/Teachers'
import Students from '@containers/Students'
import ActivityLog from '@containers/ActivityLog'

import Navigation from '@containers/Navigation'
import Sidebar from '@containers/Sidebar'

import MainPagesStyles from './MainPagesStyled'

function MainPages({ actions }) {
  const classes = MainPagesStyles()
  const [menu, setMenu] = useState('orders')
  const handleChange = (menu) => {
    setMenu(menu)
    actions.setSelectTeacherId('')
  }

  return (
    <div className={classes.root}>
      <Navigation menu={menu} onChange={handleChange} />
      <Grid container>
        <Sidebar menu={menu} onChange={handleChange} />
        <div className={classes.main}>
          <div style={{ height: '72px' }} />
          {menu === 'orders' && <Orders />}
          {menu === 'teachers' && <Teachers />}
          {menu === 'students' && <Students />}
          {menu === 'activity' && <ActivityLog />}
        </div>
      </Grid>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(MainPages)
