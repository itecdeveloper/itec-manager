import { makeStyles } from '@material-ui/core/styles'

const NavigationStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  toolBar: {
    minHeight: 72,
  },
  appBar: {
    boxShadow: 'none',
  },
  buttonRoot: {
    display: 'block',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  iconButtonRoot: {
    display: 'none',
    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
  },
  popperFullWidth: {
    width: '100%',
    left: 0,
    top: 0,
    transform: 'translate3d(0px, 72px, 0px)',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: 0,
    opacity: ({ opacity }) => (opacity ? 1 : 0),
    transition: ({ opacity }) => (opacity ? 'opacity 0.5s' : 'opacity 0.8s'),
  },
  paper: {
    boxShadow: '0 2px 4px 0 #9B9B9B',
    borderRadius: '0px 0px 4px 4px',
  },
  mobileRegisterRoot: {
    padding: 24,
  },
  mobileLoginRoot: {
    textAlign: 'center',
    marginTop: 24,
    fontSize: '16px',
    lineHeight: '24px',
    '& .text': {
      color: 'rgba(122, 122, 122, 0.7)',
    },
    '& .text-login': {
      marginLeft: 5,
      color: theme.palette.primary.main,
      fontWeight: 'bold',
    },
  },
  iconButtonMargin: {
    margin: theme.spacing(1),
  },
  popperAccount: {
    // width: '100%',
    left: 0,
    top: 0,
    transform: 'translate3d(0px, 72px, 0px)',
    bottom: 0,
  },
  paperAccount: {
    boxShadow: 'inset 0px 0px 4px rgba(0, 0, 0, 0.12)',
    borderRadius: '4px',
    minWidth: 248,
  },
  account: {
    paddingBottom: '17px',
    borderBottom: '1px solid #DEDEDE',
    '& .title': {
      fontWeight: 600,
      fontSize: '18px',
      lineHeight: '28px',
      letterSpacing: '0.02em',
      color: 'rgba(0, 0, 0, 0.7)',
    },
    '& .email': {
      fontSize: '14px',
      lineHeight: '20px',
      letterSpacing: '0.02em',
      color: 'rgba(122, 122, 122, 0.7)',
    },
  },
  muiButton: {
    paddingLeft: 0,
    width: '100%',
    justifyContent: 'end',
    fontSize: '1rem',
    textTransform: 'capitalize',
  },
  iconButtonMenu: {
    marginRight: '12px',
    fontSize: '1.25rem',
    color: '#CCCCCC',
  },
}))

export default NavigationStyles
