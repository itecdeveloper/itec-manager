import { makeStyles } from '@material-ui/core/styles'

const BookingStyle = makeStyles((theme) => ({
  root: {
    width: 375,
    fontSize: '14px',
    lineHeight: 1.25,
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.8)',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  infoSection: {
    padding: '6px 0',
  },
  subtitle2: {
    fontWeight: 600,
    lineHeight: '1.25rem',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  body2: {
    color: 'rgba(0, 0, 0, 0.5)',
  },
  buttonSection: {
    marginTop: '24px',
  },
  buttonCancel: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  boxInputTime: {
    border: '1px solid #CCC',
    borderRadius: '5px',
  },
  boxInputTimeDisabled: {
    border: '1px solid #E1E1E1',
  },
  labelTime: {
    padding: '8px 15px 4px 15px',
    fontSize: '0.75rem',
    lineHeight: '1rem',
    color: '#808080',
  },
  alertRoot: {
    marginBottom: '24px',
  },
  alert: {
    color: theme.palette.primary.main,
    backgroundColor: '#FDE4E5',
    boxShadow: 'none',
    borderRadius: '5px',
  },
  alertIcon: {
    alignItems: 'center',
  },
}))

export default BookingStyle
