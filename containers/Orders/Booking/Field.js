import Autocomplete from '@components/Autocomplete'
import AutocompleteTime from '@components/AutocompleteTime'
import InputDate from '@components/InputDate'

export function CustomInputBookingType({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'type')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}
export function CustomInputDate({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'date')
  }
  return <InputDate onChange={handleChange} {...rest} />
}
export function CustomInputTimeHour({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'time_hour')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}
export function CustomInputTimeMinute({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'time_min')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}
export function CustomInputSession({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'session')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}
