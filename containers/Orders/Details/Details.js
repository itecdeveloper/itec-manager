import React, { Fragment, useState } from 'react'
import moment from 'moment'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Button from '@components/Button'
import CircularLoading from '@components/CircularLoading'
import { paymentStatusOptions, showOrderPaymentType } from '@util/orderFunc'
import { filterSessionActivate } from '@util/filterData'
import { displayShortID, displayStudentShortID } from '@util/centerFunc'
import { compareValues } from '@util/formatData'

import {
  CustomInputLevel,
  CustomInputTeacher,
  CustomInputStatus,
  CustomInputPaymentDetail,
  CustomInputSession,
} from './Field'
import detailsStyle from './DetailsStyled'

function Details({ onClose, detail, isLoading, actions, levels, teachers }) {
  const classes = detailsStyle()
  const paymentStatus = paymentStatusOptions()
  const indexStatus = paymentStatus.findIndex((status) => status.value === detail.STATUS)
  const [state, setState] = useState({
    LEVEL_ID: detail.LEVEL_ID === '-' ? '' : { label: detail.LEVEL, value: detail.LEVEL_ID },
    TEACHER_ID:
      detail.TEACHER_ID === '-' ? '' : { label: detail.TEACHER, value: detail.TEACHER_ID },
    STATUS: detail.status !== 'undefined' ? paymentStatus[indexStatus] : { label: '', value: '' },
    PAYMENT_DETAIL: detail.PAYMENT_DETAIL,
    SESSION_QUANTITY: detail.SESSION_QUANTITY,
  })
  const [errors, setErrors] = useState({ status: false, message: '' })
  const [sessionErrors, setSessionErrors] = useState({ status: false, message: '' })
  const sessionNow = filterSessionActivate(detail.SESSIONS)
  const onChange = async (event, name) => {
    let data = {}
    if (name === 'LEVEL_ID') {
      let levelTeachers = levels.filter((level) => level.value === event.value)
      await actions.setOrderDetailTeachers(levelTeachers[0].teachers)
      data = {
        ...state,
        [name]: event,
        TEACHER_ID: '',
      }
    } else if (name === 'TEACHER_ID') {
      data = {
        ...state,
        [name]: event,
      }
    } else if (name === 'STATUS') {
      if (event.value === 'PAID' && detail.PAYMENT_TYPE === 'bank_tran') {
        if (state.PAYMENT_DETAIL > 0) {
          setErrors({ status: false, message: '' })
        } else {
          setErrors({ status: true, message: 'This field is a required' })
        }
      } else {
        setErrors({ status: false, message: '' })
      }
      data = {
        ...state,
        [name]: event,
        PAYMENT_DETAIL: '',
      }
    } else if (name === 'PAYMENT_DETAIL') {
      if (state.STATUS.value === 'PAID' && detail.PAYMENT_TYPE === 'bank_tran') {
        if (event.length > 0) {
          setErrors({ status: false, message: '' })
        } else {
          setErrors({ status: true, message: 'This field is a required' })
        }
      }
      data = {
        ...state,
        [name]: event,
      }
    } else if (name === 'SESSION_QUANTITY') {
      if (sessionNow.length > event) {
        setSessionErrors({
          status: true,
          message: 'Number of session cannot be less than the remaining session',
        })
      } else {
        setSessionErrors({ status: false, message: '' })
      }
      data = {
        ...state,
        [name]: event,
      }
    }
    setState(data)
  }
  const handleClose = () => {
    setState({
      LEVEL_ID: {},
      TEACHER_ID: {},
      STATUS: paymentStatus[indexStatus],
      PAYMENT_DETAIL: '',
    })
    onClose()
  }
  const handleSubmit = async () => {
    if (state.STATUS.value === 'PAID' && state.PAYMENT_DETAIL === '') {
      setErrors({ status: true, message: 'This field is a required' })
    }
    if (state.SESSION_QUANTITY < sessionNow.length) {
      setSessionErrors({
        status: true,
        message: 'Number of session cannot be less than the remaining session',
      })
    }
    if (!errors.status && !sessionErrors.status) {
      await actions.updateOrder(detail.ORDER_ID, state)
      onClose()
    }
  }
  levels.sort(compareValues('label', 'asc'))
  teachers.sort(compareValues('label', 'asc'))
  return (
    <div className={classes.root}>
      {isLoading ? (
        <CircularLoading />
      ) : (
        <Fragment>
          <div>
            <Typography variant="subtitle1" component="div" className={classes.subtitle1}>
              Order Info
            </Typography>
            <Grid container alignItems="center">
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Order ID
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                <span style={{ fontWeight: 600 }}>{displayShortID(detail.ORDER_ID)}</span>
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Student
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                {detail.FISRT_NAME} {detail.LAST_NAME}
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Student ID
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                {displayStudentShortID(detail.STUDENT_ID)}
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Starting
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                {moment(detail.START_TIME).format('MMMM DD, YYYY • HH:mm')}
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Course
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                {detail.COURSE_NAME}
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Session
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                <Grid container alignItems="center">
                  <Grid items lg={2}>
                    {sessionNow.length} /
                  </Grid>
                  <Grid items lg={4}>
                    <CustomInputSession
                      type="number"
                      name="SESSION_QUANTITY"
                      label="Quantity"
                      className={classes.margin}
                      fullWidth
                      outlined
                      value={state.SESSION_QUANTITY}
                      onChange={onChange}
                      error={sessionErrors.status}
                      errorText={sessionErrors.status && sessionErrors.message}
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Level
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                <CustomInputLevel
                  name="LEVEL_ID"
                  label="Level"
                  options={levels}
                  value={state.LEVEL_ID}
                  onChange={onChange}
                  fullWidth
                />
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Teacher
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                <CustomInputTeacher
                  name="TEACHER_ID"
                  label="Assign teacher"
                  options={teachers}
                  value={state.TEACHER_ID}
                  onChange={onChange}
                  disabled={!state.LEVEL_ID}
                  fullWidth
                />
              </Grid>
            </Grid>
          </div>
          <div style={{ marginTop: '28px' }}>
            <Typography variant="subtitle1" component="div" className={classes.subtitle1}>
              Payment Details
            </Typography>
            <Grid container alignItems="center">
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Method
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                {showOrderPaymentType(detail.PAYMENT_TYPE)}
              </Grid>
              <Grid items xs={3} className={classes.infoSection}>
                <Typography variant="body2" className={classes.body2}>
                  Status
                </Typography>
              </Grid>
              <Grid items xs={9} className={classes.infoSection}>
                {detail.STATUS === 'PAID' ? (
                  'Successful'
                ) : (
                  <CustomInputStatus
                    name="STATUS"
                    label="Status"
                    options={paymentStatusOptions()}
                    value={state.STATUS}
                    onChange={onChange}
                    fullWidth
                  />
                )}
              </Grid>
              {detail.PAYMENT_TYPE === 'bank_tran' ? (
                <Fragment>
                  <Grid items xs={3} className={classes.infoSection}>
                    <Typography variant="body2" className={classes.body2}>
                      Ref no.
                    </Typography>
                  </Grid>
                  <Grid items xs={9} className={classes.infoSection}>
                    <CustomInputPaymentDetail
                      name="PAYMENT_DETAIL"
                      label="Required"
                      value={state.PAYMENT_DETAIL}
                      className={classes.margin}
                      fullWidth
                      outlined
                      onChange={onChange}
                      error={errors.status}
                      errorText={errors.status && errors.message}
                    />
                  </Grid>
                </Fragment>
              ) : null}
            </Grid>
          </div>
          <div className={classes.buttonSection}>
            <Button
              type="submit"
              color="primary"
              size="large"
              fullWidth
              onClick={handleSubmit}
              disabled={errors.status || sessionErrors.status ? true : false}
            >
              Save changes
            </Button>
            <Button fullWidth className={classes.buttonCancel} onClick={handleClose}>
              Cancel
            </Button>
          </div>
        </Fragment>
      )}
    </div>
  )
}
export default Details
