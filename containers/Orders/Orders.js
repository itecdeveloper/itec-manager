import React, { useEffect, useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'

import * as Actions from '@actions/orderActions'

import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Button from '@components/Button'
import Icon from '@components/Icon'
import Search from '@components/Search'
import { filterDatatableOrders } from '@util/filterData'

import Datatable from './Datatable'
import OrdersStyles from './OrdersStyled'

function Orders({ actions, orders }) {
  const classes = OrdersStyles()
  const { allOrders, allOrderOriginal, textSearch } = orders
  const [page, setPage] = useState(1)
  const [pagePending, setPagePending] = useState(1)
  const [expanded, setExpanded] = useState({})
  const [expandedPending, setExpandedPending] = useState({})
  useEffect(() => {
    actions.initOrders()
  }, [])
  const onChangeSearch = (text) => {
    actions.setTextSearch(text)
    setPage(1)
    setPagePending(1)
    setExpanded({})
    setExpandedPending({})
    const orders = filterDatatableOrders(allOrderOriginal.orders, text)
    const pendingOrders = filterDatatableOrders(allOrderOriginal.pendingOrders, text)
    actions.setAllOrders({ orders, pendingOrders })
  }
  const handleChangePage = (newPage) => {
    setPage(newPage)
    setExpanded({})
  }
  const handleChangePagePending = (newPage) => {
    setPagePending(newPage)
    setExpandedPending({})
  }
  const handleExpanded = (id) => {
    setExpanded({ ...expanded, [id]: !expanded[id] })
  }
  const handleExpandedPending = (id) => {
    setExpandedPending({ ...expandedPending, [id]: !expandedPending[id] })
  }
  const columns = [
    { id: 'ORDER_ID', sort: true, label: 'Order ID', width: 76 },
    { id: 'STUDENT_ID', sort: true, label: 'Student ID', width: 88 },
    { id: 'STUDENT', sort: true, label: 'Student', width: 150 },
    { id: 'CONTACT_NO', sort: true, label: 'Phone', width: 116 },
    { id: 'COURSE_NAME', sort: true, label: 'Course Name', width: 200 },
    { id: 'TEACHER', sort: true, label: 'Teacher', width: 135 },
    { id: 'STATUS', sort: true, label: 'Status', width: 85 },
  ]
  return (
    <Container>
      <Grid container alignItems="center">
        <Grid item xs={12} lg={4}>
          <Typography variant="h4" className={classes.typographyH4}>
            Order
          </Typography>
        </Grid>
        <Grid item xs={12} lg={8}>
          <Grid container alignItems="center" justify="flex-end">
            <Grid item xs={12} sm={8} lg={'auto'}>
              <Search
                value={textSearch}
                onChange={(e) => onChangeSearch(e.target.value)}
                placeholder="Search Order ID, Course Name, etc "
              />
            </Grid>
            <Grid item xs={12} sm={4} lg={'auto'}>
              <Button
                color="primary"
                size="small"
                className={classes.buttonExport}
                onClick={() => actions.exportOrders()}
              >
                Export List <Icon className={classes.iconExport}>export</Icon>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <div className={classes.rootPendingOrders}>
        <Typography variant="subtitle1" className={classes.typographySubtitle1}>
          Pending action
        </Typography>
        <Datatable
          keyField="tablePendingOrders"
          columns={columns}
          data={allOrders.pendingOrders}
          expandRow="right"
          page={pagePending}
          onChangePage={handleChangePagePending}
          expanded={expandedPending}
          onClickExpand={handleExpandedPending}
        />
      </div>
      <div className={classes.rootOrders}>
        <Typography variant="subtitle1" className={classes.typographySubtitle1}>
          All Courses
        </Typography>
        <Datatable
          keyField="tableOrders"
          columns={columns}
          data={allOrders.orders}
          expandRow="right"
          page={page}
          onChangePage={handleChangePage}
          expanded={expanded}
          onClickExpand={handleExpanded}
        />
      </div>
    </Container>
  )
}
const mapStateToProps = (state) => {
  return {
    orders: state.orders,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(Orders)
