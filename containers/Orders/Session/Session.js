import React, { Fragment } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import classNames from 'classnames'
import moment from 'moment'

import * as Actions from '@actions/orderActions'

import Box from '@material-ui/core/Box'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

import Button from '@components/Button'

import { compareValues } from '@util/formatData'

import SessionStyles from './SessionStyles'

function Session({ orderDetail, actions }) {
  const classes = SessionStyles()
  const onClickStatus = async (orderId, sessionId, status) => {
    await actions.updateSession(orderId, sessionId, status, 'status')
  }
  const onClickStatusReschedule = async (orderId, sessionId, status) => {
    await actions.updateSession(orderId, sessionId, status, 'reschedule')
  }
  const onClickDeleteSession = async (orderId, sessionId) => {
    await actions.deleteSession(orderId, sessionId)
  }
  const sessions = orderDetail.SESSIONS.sort(compareValues('START_TIME', 'asc'))
  const now = moment().valueOf()
  return (
    <Box margin={1}>
      <Table size="small" aria-label="orderDetails">
        <TableBody>
          {sessions.map((session, key) => {
            const endTime = moment(session.END_TIME).valueOf()
            return (
              <Fragment>
                <TableRow key={key}>
                  <TableCell
                    component="th"
                    scope="row"
                    classes={{
                      root: classes.smRootTableCell,
                      body: classes.smBodyTableCell,
                    }}
                    style={{ width: '160px' }}
                  >
                    {moment(session.START_TIME).format('MMM DD, YYYY • HH:mm')}
                  </TableCell>
                  {session.S_STATUS === 'WAITING_APPROVE' ? (
                    <TableCell
                      classes={{
                        root: classes.smRootTableCell,
                        body: classes.smBodyTableCell,
                      }}
                    >
                      Booking request :
                      <Button
                        className={classNames({
                          [classes.buttonAction]: true,
                          [classes.buttonActionApprove]: true,
                        })}
                        onClick={() =>
                          onClickStatus(orderDetail.ORDER_ID, session.SESSION_ID, 'ACTIVE')
                        }
                      >
                        Approve
                      </Button>
                      <Button
                        className={classNames({
                          [classes.buttonAction]: true,
                          [classes.buttonActionDeny]: true,
                        })}
                        onClick={() =>
                          onClickDeleteSession(orderDetail.ORDER_ID, session.SESSION_ID)
                        }
                      >
                        Deny Request
                      </Button>
                    </TableCell>
                  ) : session.S_STATUS !== 'CANCELLED' ? (
                    session.S_STATUS === 'ACTIVE' && session.STATUS_NEW_START_TIME === 'req' ? (
                      <TableCell
                        classes={{
                          root: classes.smRootTableCell,
                          body: classes.smBodyTableCell,
                        }}
                      >
                        Reschedule request to :
                        {moment(session.NEW_START_TIME).format('MMM DD, YYYY • HH:mm')}
                        <Button
                          className={classNames({
                            [classes.buttonAction]: true,
                            [classes.buttonActionApprove]: true,
                          })}
                          onClick={() =>
                            onClickStatusReschedule(
                              orderDetail.ORDER_ID,
                              session.SESSION_ID,
                              'approve',
                            )
                          }
                        >
                          Approve
                        </Button>
                        <Button
                          className={classNames({
                            [classes.buttonAction]: true,
                            [classes.buttonActionDeny]: true,
                          })}
                          onClick={() =>
                            onClickStatusReschedule(
                              orderDetail.ORDER_ID,
                              session.SESSION_ID,
                              'reject',
                            )
                          }
                        >
                          Deny Request
                        </Button>
                      </TableCell>
                    ) : (
                      <TableCell
                        classes={{
                          root: classes.smRootTableCell,
                          body: classes.smBodyTableCell,
                        }}
                      >
                        Status :
                        <Button
                          className={classNames({
                            [classes.buttonAction]: true,
                            [classes.buttonActionComplete]: session.S_STATUS === 'COMPLETE',
                          })}
                          onClick={() =>
                            onClickStatus(orderDetail.ORDER_ID, session.SESSION_ID, 'COMPLETE')
                          }
                          disabled={now < endTime}
                        >
                          Completed
                        </Button>
                        <Button
                          className={classNames({
                            [classes.buttonAction]: true,
                            [classes.buttonActionComplete]: session.S_STATUS === 'ACTIVE',
                          })}
                          onClick={() =>
                            onClickStatus(session.ORDER_ID, session.SESSION_ID, 'ACTIVE')
                          }
                          disabled={now < endTime}
                        >
                          Incomplete
                        </Button>
                      </TableCell>
                    )
                  ) : null}
                </TableRow>
              </Fragment>
            )
          })}
        </TableBody>
      </Table>
    </Box>
  )
}
const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}
const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))
export default enhancer(Session)
