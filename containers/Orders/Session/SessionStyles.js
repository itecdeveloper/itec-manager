import { makeStyles } from '@material-ui/core/styles'

const SessionStyles = makeStyles((theme) => ({
  smRootTableCell: {
    borderBottom: 'unset',
  },
  smBodyTableCell: {
    padding: '4px 10px',
    fontSize: '0.8125rem',
    fontWeight: 'normal',
    lineHeight: 1.5,
    color: 'rgba(0, 0, 0, 0.8)',
    fontWeight: 'normal',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  buttonAction: {
    height: '24px',
    minWidth: '100px',
    marginLeft: '8px',
    fontSize: '0.8125rem',
    fontWeight: 500,
    lineHeight: '1.25rem',
    color: 'rgba(0, 0, 0, 0.6)',
    border: '1px solid #CCCCCC',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  buttonActionComplete: {
    color: '#FFF',
    backgroundColor: 'rgba(70, 181, 50, 0.7)',
    border: 'none',
    '&:hover': {
      backgroundColor: 'rgba(70, 181, 50, 0.7)',
    },
    '&.disabled': {
      backgroundColor: 'rgba(70, 181, 50, 0.5)',
      color: '#FFF',
    },
  },
  buttonActionIncomplete: {
    color: '#83610D',
    backgroundColor: '#FFEBBA',
    border: '1px solid #FFEBBA',
    '&:hover': {
      backgroundColor: '#FFEBBA',
    },
  },
  buttonActionApprove: {
    minWidth: '130px',
    color: '#FFF',
    backgroundColor: '#ED1B24',
    border: '1px solid #ED1B24',
    '&:hover': {
      backgroundColor: '#ED1B24',
    },
  },
  buttonActionDeny: {
    minWidth: '130px',
    color: '#ED1B24',
    backgroundColor: 'transparent',
    border: '1px solid #ED1B24',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
}))
export default SessionStyles
