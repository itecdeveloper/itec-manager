import React from 'react'
import moment from 'moment'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import detailsStyle from './DetailsStyled'

function Details({ detail }) {
  const classes = detailsStyle()
  const nowAge = moment().diff(moment(detail.DOB), 'years')
  return (
    <div className={classes.root}>
      <div>
        <Typography variant="subtitle1" component="div" className={classes.subtitle1}>
          Personal Info
        </Typography>
        <Grid container>
          <Grid items xs={3} className={classes.infoSection}>
            <Typography variant="body2" className={classes.body2}>
              First Name
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {detail.FISRT_NAME}
          </Grid>
          <Grid items xs={3} className={classes.infoSection}>
            <Typography variant="body2" className={classes.body2}>
              Last Name
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {detail.LAST_NAME}
          </Grid>
          <Grid items xs={3} className={classes.infoSection}>
            <Typography variant="body2" className={classes.body2}>
              Nickname
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {detail.NICK_NAME}
          </Grid>
          <Grid items xs={3} className={classes.infoSection}>
            <Typography variant="body2" className={classes.body2}>
              Date of Birth
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {moment(detail.DOB).format('MMMM DD, YYYY')}
          </Grid>
          <Grid items xs={3} className={classes.infoSection}>
            <Typography variant="body2" className={classes.body2}>
              Phone no.
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {detail.CONTACT_NO}
          </Grid>
          <Grid items xs={3} className={classes.infoSection}>
            <Typography variant="body2" className={classes.body2}>
              Line ID
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {detail.LINE || '-'}
          </Grid>
          <Grid items xs={3} className={classes.infoSection}>
            <Typography variant="body2" className={classes.body2}>
              Email
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {detail.EMAIL}
          </Grid>
          <Grid items xs={3}>
            <Typography variant="body2" className={classes.body2}>
              School / Company Name
            </Typography>
          </Grid>
          <Grid items xs={9} className={classes.infoSection}>
            {detail.SC_NAME || '-'}
          </Grid>
        </Grid>
      </div>
      {nowAge <= 15 ? (
        <div style={{ marginTop: '28px' }}>
          <Typography variant="subtitle1" component="div" className={classes.subtitle1}>
            Contact Person
          </Typography>
          <Grid container>
            <Grid items xs={3} className={classes.infoSection}>
              <Typography variant="body2" className={classes.body2}>
                First Name
              </Typography>
            </Grid>
            <Grid items xs={9} className={classes.infoSection}>
              {detail.CP_FISRT_NAME || '-'}
            </Grid>
            <Grid items xs={3} className={classes.infoSection}>
              <Typography variant="body2" className={classes.body2}>
                Last Name
              </Typography>
            </Grid>
            <Grid items xs={9} className={classes.infoSection}>
              {detail.CP_LAST_NAME || '-'}
            </Grid>
            <Grid items xs={3} className={classes.infoSection}>
              <Typography variant="body2" className={classes.body2}>
                Relationship
              </Typography>
            </Grid>
            <Grid items xs={9} className={classes.infoSection}>
              {detail.RELATIONSHIP || '-'}
            </Grid>
            <Grid items xs={3} className={classes.infoSection}>
              <Typography variant="body2" className={classes.body2}>
                Email
              </Typography>
            </Grid>
            <Grid items xs={9} className={classes.infoSection}>
              {detail.CP_EMAIL || '-'}
            </Grid>
            <Grid items xs={3} className={classes.infoSection}>
              <Typography variant="body2" className={classes.body2}>
                Phone no.
              </Typography>
            </Grid>
            <Grid items xs={9} className={classes.infoSection}>
              {detail.CP_CONTACT_NO || '-'}
            </Grid>
            <Grid items xs={3} className={classes.infoSection}>
              <Typography variant="body2" className={classes.body2}>
                Line ID
              </Typography>
            </Grid>
            <Grid items xs={9} className={classes.infoSection}>
              {detail.CP_LINE || '-'}
            </Grid>
          </Grid>
        </div>
      ) : null}
    </div>
  )
}
export default Details
