import { makeStyles } from '@material-ui/core/styles'

const detailsStyle = makeStyles((theme) => ({
  root: {
    width: 375,
    fontSize: '14px',
    lineHeight: 1.25,
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.8)',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  infoSection: {
    padding: '6px 0',
  },
  subtitle1: {
    fontWeight: 600,
    lineHeight: 1.5,
    color: 'rgba(0, 0, 0, 0.7)',
  },
  body2: {
    color: 'rgba(0, 0, 0, 0.5)',
  },
}))

export default detailsStyle
