import React, { useEffect, useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'

import * as Actions from '@actions/studentActions'

import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Button from '@components/Button'
import Icon from '@components/Icon'
import Search from '@components/Search'
import { filterDatatableStudents } from '@util/filterData'

import Datatable from './Datatable'
import StudentsStyles from './StudentsStyled'

function Students({ actions, allStudents, allStudentOriginal, isLoading }) {
  const classes = StudentsStyles()
  const [textSearch, setTextSearch] = useState('')
  const [page, setPage] = useState(1)
  useEffect(() => {
    actions.initStudent()
  }, [])
  const onChangeSearch = (event) => {
    const text = event.target.value
    setTextSearch(text)
    setPage(1)
    const newData = filterDatatableStudents(allStudentOriginal, text)
    actions.setAllStudents(newData)
  }
  const handleChangePage = (page) => {
    setPage(page)
  }
  const columns = [
    { id: 'ACCOUNT_ID', sort: true, label: 'Student ID' },
    { id: 'FISRT_NAME', sort: true, label: 'Name' },
    { id: 'NICK_NAME', sort: true, label: 'Nickname' },
    { id: 'EMAIL', sort: true, label: 'Email' },
    { id: 'CONTACT_NO', sort: true, label: 'Phone' },
  ]
  return (
    <Container>
      <Grid container alignItems="center">
        <Grid item xs={12} lg={4}>
          <Typography variant="h4" className={classes.typographyH4}>
            Students
          </Typography>
        </Grid>
        <Grid item xs={12} lg={8}>
          <Grid container alignItems="center" justify="flex-end">
            <Grid item xs={12} sm={8} lg={'auto'}>
              <Search
                value={textSearch}
                onChange={onChangeSearch}
                placeholder="Search Student Name, ID, email, etc."
              />
            </Grid>
            <Grid item xs={12} sm={4} lg={'auto'}>
              <Button
                color="primary"
                size="small"
                className={classes.buttonExport}
                onClick={() => {
                  actions.exportStudents()
                }}
              >
                Export List <Icon style={{ marginLeft: 10, fontSize: 18 }}>export</Icon>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <div className={classes.rootDatatable}>
        <Datatable
          keyField="tableStudents"
          columns={columns}
          data={allStudents}
          expandRow="right"
          page={page}
          onChangePage={handleChangePage}
        />
      </div>
    </Container>
  )
}

const mapStateToProps = (state) => {
  return {
    allStudents: state.student.allStudents,
    allStudentOriginal: state.student.allStudentOriginal,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(Students)
