import { makeStyles } from '@material-ui/core/styles'

const StudentsStyles = makeStyles((theme) => ({
  typographyH4: {
    color: 'rgba(0, 0, 0, 0.7)',
    fontSize: '2rem',
    fontWeight: 'normal',
    lineHeight: 2.4375,
    flexGrow: 1,
  },
  buttonExport: {
    minWidth: '180px',
    height: '48px',
    marginLeft: '12px',
    [theme.breakpoints.between('sm', 'md')]: {
      width: '-webkit-fill-available',
      marginLeft: '12px',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100%',
      marginLeft: 0,
      marginTop: '8px',
    },
  },
  rootDatatable: {
    marginTop: '16px',
  },
}))

export default StudentsStyles
