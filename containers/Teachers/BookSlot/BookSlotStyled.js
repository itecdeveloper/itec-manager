import { makeStyles } from '@material-ui/core/styles'

const BookSlotStyle = makeStyles((theme) => ({
  root: {
    width: 375,
    fontSize: '14px',
    lineHeight: 1.25,
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.8)',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  infoSection: {
    padding: '6px 0',
  },
  infoSection2: {
    padding: '4px 0',
  },
  body2: {
    color: 'rgba(0, 0, 0, 0.5)',
  },
  info: {
    lineHeight: '1.5rem',
    color: 'rgba(0, 0, 0, 0.7)',
    fontWeight: 'normal',
  },
  infoBold: {
    fontWeight: 600,
  },
  boxInputTime: {
    border: '1px solid #CCC',
    borderRadius: '5px',
  },
  boxInputTimeDisabled: {
    border: '1px solid #E1E1E1',
  },
  labelTime: {
    padding: '4px 15px',
    fontSize: '12px',
    lineHeight: '16px',
    color: '#808080',
  },
  rootSuccess: {
    width: '375px',
  },
  section1: {
    padding: '32px 24px 24px 24px',
    borderBottom: '1px solid #E1E1E1',
  },
  section2: {
    padding: '24px',
    fontSize: '0.8125rem',
    lineHeight: '1.25rem',
    color: 'rgba(0, 0, 0, 0.5)',
  },
  sectionIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '16px',
  },
  rounedIcon: {
    width: '50px',
    height: '50px',
    backgroundColor: '#ED1B24',
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '10px',
  },
  subtitle1: {
    lineHeight: '1.5rem',
    fontWeight: 600,
    color: '#ED1B24',
    textAlign: 'center',
  },
  notedText: {
    marginTop: '8px',
    fontWeight: 'normal',
    lineHeight: '1.25rem',
    color: 'rgba(0, 0, 0, 0.6)',
    textAlign: 'center',
  },
  title: {
    lineHeight: '1.25rem',
    fontWeight: 600,
    color: 'rgba(0, 0, 0, 0.7)',
  },
  inputText: {
    fontSize: '1rem',
    fontWeight: 500,
    lineHeight: '1.5rem',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  buttonDone: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
}))
export default BookSlotStyle
