import Autocomplete from '@components/Autocomplete'
import AutocompleteTime from '@components/AutocompleteTime'
import Input from '@components/Input'
import InputDate from '@components/InputDate'

export function CustomInputTitle({ onChange, ...rest }) {
  const handleChange = (event) => {
    onChange(event.target.value, 'TITLE')
  }
  return <Input onChange={handleChange} {...rest} />
}

export function CustomInputBookingType({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'TYPE')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}

export function CustomInputStartDate({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'START_DATE')
  }
  return <InputDate onChange={handleChange} {...rest} />
}
export function CustomInputStartTimeHour({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'START_TIME_HOUR')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}
export function CustomInputStartTimeMinute({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'START_TIME_MIN')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}
export function CustomInputEndDate({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'END_DATE')
  }
  return <InputDate onChange={handleChange} {...rest} />
}
export function CustomInputEndTimeHour({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'END_TIME_HOUR')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}
export function CustomInputEndTimeMinute({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'END_TIME_MIN')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}
