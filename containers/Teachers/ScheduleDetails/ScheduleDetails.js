import React, { Fragment, useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'

import * as Actions from '@actions/teacherActions'

import BookSlotRemove from './BookSlotRemove'
import ViewSession from './ViewSession'

import Modal from '@components/Modal'
import Icon from '@components/Icon'
import IconRounded from '@components/IconRounded'
import { compareValues } from '@util/formatData'

import ScheduleDetailsStyles from './ScheduleDetailsStyles'

function ScheduleDetails({
  details,
  actions,
  dataRemove,
  selectCalendar,
  selectTeacher,
  viewSession,
  studentDetails,
}) {
  const classes = ScheduleDetailsStyles()
  const [open, setOpen] = useState(false)
  const [openView, setOpenView] = useState(false)
  const handleOpenRemove = async (lastLink) => {
    let detail = selectCalendar.filter((calendar) => {
      return calendar.LAST_LINK === lastLink
    })
    await actions.setRemoveBookSlot(detail[0])
    setOpen((prev) => !prev)
  }
  const handleOpenViewSession = async (index) => {
    let student = studentDetails.filter((student) => {
      return student.ACCOUNT_ID === selectCalendar[index].STUDENT_ID
    })
    if (student.length === 0) {
      await actions.getStudentCourse(selectCalendar[index].STUDENT_ID)
    }
    await actions.setViewSession(selectCalendar[index])
    setOpenView((prev) => !prev)
  }
  return (
    <Fragment>
      <div className={classes.rootScheduleDetails}>
        {details && details.length > 0 ? (
          details.map((detail, index) =>
            detail.SHOW_TYPE === 'COURSE' ? (
              <div key={index} className={classes.sectionScheduleDetails}>
                <div className="time">
                  {`${detail.START_TIME_FORMATE.time.hour}:${detail.START_TIME_FORMATE.time.min}`}
                  <br />
                  {`${detail.END_TIME_FORMATE.time.hour}:${detail.END_TIME_FORMATE.time.min}`}
                </div>
                <div
                  style={{ cursor: 'pointer' }}
                  className={classes.sectionDetails}
                  onClick={() => {
                    handleOpenViewSession(index)
                  }}
                >
                  <div>
                    <span className="title">
                      {detail.MY_COUSE.COURSE_DETAIL.DETAIL.COURSE_NAME}
                    </span>
                  </div>
                  <div className="description">{detail.NOTE}</div>
                </div>
              </div>
            ) : (
              <div key={index} className={classes.sectionScheduleDetails}>
                <div className="time">
                  {`${detail.START_TIME_FORMATE.time.hour}:${detail.START_TIME_FORMATE.time.min}`}
                  <br />
                  {`${detail.END_TIME_FORMATE.time.hour}:${detail.END_TIME_FORMATE.time.min}`}
                </div>
                <div
                  style={{ cursor: 'pointer' }}
                  className={classes.sectionDetails}
                  onClick={() => {
                    handleOpenRemove(detail.LAST_LINK)
                  }}
                >
                  <div>
                    <span className="title">{detail.TITLE}</span>
                  </div>
                </div>
              </div>
            ),
          )
        ) : (
          <div className={classes.noEventRoot}>
            <div>No Event</div>
          </div>
        )}
      </div>
      <Modal
        open={open}
        onClose={() => {
          setOpen((prev) => !prev)
        }}
        headerChildren={<IconRounded title="Book Slot" icon={<Icon>plus</Icon>} />}
      >
        <BookSlotRemove
          actions={actions}
          data={dataRemove}
          selectTeacher={selectTeacher}
          onClose={() => {
            setOpen((prev) => !prev)
          }}
        />
      </Modal>
      <Modal
        open={openView}
        onClose={() => {
          setOpenView((prev) => !prev)
        }}
        headerChildren={<IconRounded title="Session Details" icon={<Icon>doc</Icon>} />}
      >
        <ViewSession data={viewSession} studentDetails={studentDetails} />
      </Modal>
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    dataRemove: state.teacher.bookSlotRemove,
    selectCalendar: state.teacher.selectCalendar,
    selectTeacher: state.teacher.selectTeacher,
    viewSession: state.teacher.viewSession,
    studentDetails: state.teacher.studentDetails,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(ScheduleDetails)
