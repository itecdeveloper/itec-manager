import { makeStyles } from '@material-ui/core/styles'

const ScheduleDetails = makeStyles((theme) => ({
  root: {},
  rootScheduleDetails: {
    marginTop: '16px',
    height: '-webkit-fill-available',
    overflowY: 'auto',
  },
  sectionScheduleDetails: {
    display: 'flex',
    paddingBottom: '12px',
    fontSize: '0.875rem',
    lineHeight: '1.5rem',
    '& .time': {
      padding: '12px 16px',
      color: 'rgba(0, 0, 0, 0.6)',
    },
  },
  sectionDetails: {
    width: '100%',
    background: '#F8F7F7',
    borderRadius: '4px',
    padding: '12px 16px',
    '& .title': {
      fontWeight: 600,
      color: 'rgba(0, 0, 0, 0.7)',
    },
    '& .alert': {
      backgroundColor: '#FDE8E9',
      borderRadius: '5px',
      color: ' #ED1B24',
      padding: '2px 8px',
      marginLeft: '8px',
    },
    '& .description': {
      lineHeight: '1.25rem',
      color: 'rgba(0, 0, 0, 0.5)',
    },
  },
  noEventRoot: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '250px',
    fontSize: '1.5rem',
    color: '#CCC',
  },
  rootRemove: {
    width: '375px',
    fontSize: '0.8125rem',
    lineHeight: '1.25rem',
    color: 'rgba(0, 0, 0, 0.5)',
  },
  infoSection: {
    padding: '6px 0',
  },
  infoSection2: {
    padding: '4px 0',
  },
  info: {
    lineHeight: '1.5rem',
    color: 'rgba(0, 0, 0, 0.7)',
    fontWeight: 'normal',
  },
  infoBold: {
    fontWeight: 600,
  },
  buttonRemove: {
    fontSize: '0.875rem',
    marginTop: '24px',
    '&.outlined:hover': {
      backgroundColor: 'transparent',
    },
  },
  titleCourse: {
    fontSize: '1.25rem',
    lineHeight: '1.75rem',
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.8)',
    marginBottom: '16px',
  },
  links: {
    textDecoration: 'none',
    '& span': {
      color: '#ED1B24',
    },
  },
  lastText: {
    color: 'rgba(0, 0, 0, 0.8)',
  },
}))

export default ScheduleDetails
