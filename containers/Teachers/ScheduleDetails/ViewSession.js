import React from 'react'
import classNames from 'classnames'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import { toDateTimeFormat } from '@util/formatDate'
import ScheduleDetailsStyles from './ScheduleDetailsStyles'

function ViewSession({ data, studentDetails }) {
  const classes = ScheduleDetailsStyles()
  const detail = data.MY_COUSE.COURSE_DETAIL
  const startDate = toDateTimeFormat(data.START_TIME)
  const endDate = toDateTimeFormat(data.END_TIME)
  const level = detail.LEVEL.find((i) => i.LEVEL_ID === data.LEVEL_ID)
  const teacher = detail.ACCOUNTS.find((account) => account.ACCOUNT_ID === data.TEACHER_ID)
  const student = studentDetails.find((account) => account.ACCOUNT_ID === data.STUDENT_ID)
  return (
    <div className={classes.root}>
      <div className={classes.rootRemove}>
        <Grid container>
          <Grid items xs={12}>
            <div className={classes.titleCourse}>
              {data.MY_COUSE.COURSE_DETAIL.DETAIL.COURSE_NAME}
            </div>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Time
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography
              variant="body2"
              className={classNames({
                [classes.info]: true,
                [classes.infoBold]: true,
              })}
            >
              {`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Level
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {level?.LEVEL || '-'}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Teacher
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {teacher ? `${teacher?.FISRT_NAME} ${teacher?.LAST_NAME}` : '-'}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Student
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {`${student?.FISRT_NAME} ${student?.LAST_NAME}`}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Note
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {data.NOTE}
            </Typography>
          </Grid>
          <Grid items xs={3} className={classes.infoSection2}>
            Links
          </Grid>
          <Grid items xs={9} className={classes.infoSection2}>
            <Typography variant="body2" className={classes.info}>
              {data.LINKS.length > 0
                ? data.LINKS.map((link, key) => (
                    <Grid key={key} style={{ paddingBottom: 8 }} item xs={12}>
                      <a href={link} target="_blank" className={classes.links}>
                        <span className={classes.lastText}>{link}</span>
                      </a>
                    </Grid>
                  ))
                : '-'}
            </Typography>
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

export default ViewSession
