import React, { Fragment, useEffect, useState } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import dynamic from 'next/dynamic'
import moment from 'moment'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import Paper from '@material-ui/core/Paper'
import Calendar from '@components/Calendar'

import * as Actions from '@actions/teacherActions'

import Button from '@components/Button'
import Icon from '@components/Icon'

import ScheduleDetails from './ScheduleDetails'
import BookSlot from './BookSlot'

import TeachersStyles from './TeachersStyled'

const BackdropLoading = dynamic(() => import('@components/BackdropLoading'), {
  ssr: false,
  loading: () => null,
})

function Teachers({ actions, isLoading, teachers }) {
  const { allTeachers, selectTeacher, calendars, selectCalendar, thisTeacherId } = teachers
  const classes = TeachersStyles()
  const [open, setOpen] = useState(false)
  const [startDate, setStartDate] = useState(null)
  useEffect(() => {
    actions.initTeachers()
    setStartDate(moment().format('D MMM YYYY'))
  }, [])
  const onClickTeacher = async (teacherId) => {
    if (teacherId !== '') {
      let detail = allTeachers.filter((teacher) => {
        return teacher.ACCOUNT_ID === teacherId
      })
      await actions.setSessionReschedule(detail[0])
      await actions.initScheduleTeacher(teacherId)
      await actions.getOrderByTeacherId(teacherId)
    }
    actions.setSelectTeacherId(teacherId)
  }
  const onClickReschedule = async (orderId, sessionId, status) => {
    let data = { orderId, sessionId, status }
    await actions.updateSessionReschedule(data)
  }
  const handleChangeDate = (value) => {
    actions.selectCalendar(value)
    setStartDate(value)
  }
  return (
    <Container>
      {thisTeacherId !== '' ? (
        <Fragment>
          <div className={classes.backSection}>
            <a onClick={() => onClickTeacher('')} style={{ cursor: 'pointer' }}>
              <Icon className={classes.iconBack}>smArrowBack</Icon>All Teachers
            </a>
          </div>
          <Paper className={classes.paper}>
            <div style={{ padding: '24px' }}>
              <Grid container alignItems="center">
                <Grid items xs={12} sm={7} md={8} lg={8}>
                  <Typography variant="h5" className={classes.typographyH5}>
                    {selectTeacher
                      ? `${selectTeacher.FISRT_NAME} ${selectTeacher.LAST_NAME}`
                      : null}
                  </Typography>
                  <Typography variant="caption" className={classes.typographyCaption}>
                    {selectTeacher ? selectTeacher.CONTACT_NO : null}
                  </Typography>
                </Grid>
                <Grid items xs={12} sm={5} md={4} lg={4} className={classes.sectionButton1}>
                  <Button
                    color="primary"
                    size="small"
                    className={classes.buttonBookSlot}
                    onClick={() => {
                      setOpen((prev) => !prev)
                    }}
                  >
                    Book Slot <Icon className={classes.iconButton}>plus</Icon>
                  </Button>
                </Grid>
              </Grid>
            </div>
            {/** ### RE-Schedule SECTION ### */}
            {/* {selectTeacher && selectTeacher.RESCHEDULE.length > 0 && (
              <div className={classes.rescheduleSection}>
                <IconRounded title="Pending Request" icon={<Icon>clock</Icon>} />
                <div className={classes.rescheduleInfoSection}>
                  {selectTeacher.RESCHEDULE.map((item, key) => {
                    return (
                      <div key={key} className={classes.rescheduleInfo}>
                        <Grid container>
                          <Grid items xs={12} sm={8} md={7} lg={9}>
                            <Typography variant="body1" className={classes.typographyBody1}>
                              {item.courseName}
                            </Typography>
                            <Typography variant="body2" className={classes.typographyBody2}>
                              {moment(parseInt(item.dateTime)).format('ddd, MMM DD • HH:mm')}
                              <Icon className={classes.iconForward}>smArrowForward</Icon>
                              <span className={classes.newSchedule}>
                                {moment
                                  .utc(parseInt(item.newDateTime))
                                  .format('ddd, MMM DD • HH:mm')}
                              </span>
                            </Typography>
                            <Typography variant="caption" className={classes.typographyCaption}>
                              Requested by {item.studentName} ( {item.studentContact} )
                            </Typography>
                          </Grid>
                          <Grid items xs={12} sm={4} md={5} lg={3} style={{ alignSelf: 'center' }}>
                            <Grid container className={classes.buttonReschedule} justify="flex-end">
                              <Grid items xs={6} sm={'auto'} md={'auto'} lg={'auto'}>
                                <Button
                                  color="primary"
                                  outlined
                                  size="small"
                                  className={classes.buttonDeny}
                                  onClick={() =>
                                    onClickReschedule(item.orderId, item.sessionId, 'reject')
                                  }
                                >
                                  Deny
                                </Button>
                              </Grid>
                              <Grid items xs={6} sm={'auto'} md={'auto'} lg={'auto'}>
                                <Button
                                  outlined
                                  size="small"
                                  className={classes.buttonApprove}
                                  onClick={() =>
                                    onClickReschedule(item.orderId, item.sessionId, 'approve')
                                  }
                                >
                                  Approve
                                </Button>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </div>
                    )
                  })}
                </div>
              </div>
            )} */}
          </Paper>
          <div className={classes.section}>
            <Grid container alignItems="center">
              <Grid items xs={12} md={5} lg={4}>
                <div className={classes.scheduleBox} style={{ marginRight: '8px' }}>
                  <Calendar value={startDate} onChange={handleChangeDate} options={calendars} />
                </div>
              </Grid>
              <Grid items xs={12} md={7} lg={8}>
                <div className={classes.scheduleBox} style={{ marginLeft: '8px' }}>
                  <div className={classes.sectionTitle}>
                    {moment(startDate).format('ddd, D MMMM YYYY')}
                  </div>
                  <ScheduleDetails details={selectCalendar} />
                </div>
              </Grid>
            </Grid>
          </div>
          <BookSlot open={open} onClose={() => setOpen((prev) => !prev)} detail={selectTeacher} />
        </Fragment>
      ) : (
        <Fragment>
          <Grid container alignItems="center">
            <Grid item xs={12}>
              <Typography variant="h4" className={classes.typographyH4}>
                Teacher’s Schedule
              </Typography>
            </Grid>
          </Grid>
          <div className={classes.section}>
            <Grid container alignItems="center" spacing={2}>
              {allTeachers.map((teacher, key) => {
                return (
                  <Grid key={key} item xs={12} sm={6} md={6} lg={4}>
                    <Card
                      className={classes.root}
                      onClick={() => onClickTeacher(teacher.ACCOUNT_ID)}
                    >
                      <CardHeader
                        classes={{ root: classes.header, action: classes.action }}
                        // action={
                        //   teacher.RESCHEDULE.length > 0 ? (
                        //     <div className={classes.actionContent}>{teacher.RESCHEDULE.length}</div>
                        //   ) : null
                        // }
                        title={
                          <Typography variant="subtitle1" className={classes.typographySubtitle1}>
                            {`${teacher.FISRT_NAME} ${teacher.LAST_NAME}`}
                          </Typography>
                        }
                      />
                    </Card>
                  </Grid>
                )
              })}
            </Grid>
          </div>
        </Fragment>
      )}
      {isLoading && <BackdropLoading open={isLoading} />}
    </Container>
  )
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading.allTeachersLoading,
    teachers: state.teacher,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(Teachers)
