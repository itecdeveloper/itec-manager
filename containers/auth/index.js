import React, { useContext } from 'react'
import useAuth from '@config/useAuth'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'

export const userContext = React.createContext({
  isAuthenticated: undefined,
})

export function useMember() {
  return useContext(userContext)
}

export function withAuth(PageComponent) {
  function EnhancedPageComponent(props) {
    // const userData = useAuth()
    const { account } = props
    const userData = account
    console.log('withAuth', { userData })

    return (
      <userContext.Provider value={userData}>
        <PageComponent {...props} />
      </userContext.Provider>
    )
  }

  EnhancedPageComponent.getInitialProps = async function (ctx) {
    let pageProps = {}
    if (typeof PageComponent.getInitialProps === 'function') {
      pageProps = await PageComponent.getInitialProps(ctx)
    }
    return pageProps
  }

  const mapStateToProps = (state) => {
    return {
      account: state.account,
    }
  }

  const mapDispatchToProps = (dispatch) => {
    return {}
  }

  return nextConnect(mapStateToProps, mapDispatchToProps)(EnhancedPageComponent)
}
