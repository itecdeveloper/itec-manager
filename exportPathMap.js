module.exports = function () {
  return {
    '/': {
      page: '/index',
    },
    '/about': {
      page: '/static-about',
    },
  }
}
