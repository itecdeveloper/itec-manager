import React, { Fragment } from 'react'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  '@global': {
    'html, body': {
      height: '100%',
      margin: 0,
      letterSpacing: '0.2px',
      fontFamily: `'Inter', sans-serif`,
    },
  },
})

export default function GlobalCss() {
  useStyles()

  return <Fragment />
}
