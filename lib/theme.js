import { createMuiTheme } from '@material-ui/core/styles'
import { red } from '@material-ui/core/colors'

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ED1B24',
    },
    secondary: {
      main: '#FFFFFF',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#FFFFFF',
    },
  },
  typography: {
    fontFamily: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
    fontSize: 14,
    caption: {
      color: '#9B9B9B',
      fontFamily: ['Inter', 'Helvetica', 'Arial', 'sans-serif'].join(','),
      fontSize: '13px',
      lineHeight: '16px',
      letterSpacing: '0.29px',
    },
    useNextVariants: true,
  },
})

export default theme
