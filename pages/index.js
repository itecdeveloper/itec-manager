import React, { Fragment } from 'react'
import MainPages from '@containers/MainPages'
import withPage from '@containers/HigherOrderComponent/withPage'
import { useMember } from '@containers/auth'

function Home() {
  const { isAuthenticated } = useMember()

  return <Fragment>{isAuthenticated && <MainPages />}</Fragment>
}

export default withPage({ restricted: true })(Home)
