const initState = {
  allActivity: {},
  allActivityOriginal: {},
  courses: [],
}
const activity = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ALL_ACTIVITY':
      return {
        ...state,
        allActivity: action.allActivity,
      }
    case 'SET_ALL_ACTIVITY_ORIGINAL':
      return {
        ...state,
        allActivityOriginal: action.allActivityOriginal,
      }
    case 'SET_COURSE_ACTIVITY':
      return {
        ...state,
        courses: action.courses,
      }
    default:
      return state
  }
}

export default activity
