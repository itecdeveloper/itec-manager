const initState = {
  open: false,
  text: 'This is an error message!',
  type: 'error',
}

const isAlert = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ALERT':
      return {
        ...state,
        open: true,
        ...action.handleAlert,
      }
    case 'CLEAR_ALERT':
      return {
        ...state,
        open: action.open,
      }
    case 'CLICK_AWAY':
      return {
        ...state,
      }
    default:
      return state
  }
}

export default isAlert
