const initState = {
  accessAccount: false,
  allCoursesLoading: false,
  courseLoading: false,
  allOrdersLoading: false,
  allStudentsLoading: false,
  allTeachersLoading: false,
  bookSlotLoading: false,
  allActivityLoading: false,
  activityDetailLoading: false,
}
const isLoading = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ACCESS_ACCOUNT_LOADING':
      return {
        ...state,
        accessAccount: action.isLoading,
      }
    case 'SET_ALL_COURSES_LOADING':
      return {
        ...state,
        allCoursesLoading: action.isLoading,
      }
    case 'SET_COURSE_LOADING':
      return {
        ...state,
        courseLoading: action.isLoading,
      }
    case 'SET_ALL_ORDERS_LOADING':
      return {
        ...state,
        allOrdersLoading: action.isLoading,
      }
    case 'SET_ORDER_DETAIL_LOADING':
      return {
        ...state,
        orderDetailLoading: action.isLoading,
      }

    case 'SET_ALL_STUDENTS_LOADING':
      return {
        ...state,
        allStudentsLoading: action.isLoading,
      }
    case 'SET_ALL_TEACHERS_LOADING':
      return {
        ...state,
        allTeachersLoading: action.isLoading,
      }
    case 'SET_BOOK_SLOT_LOADING':
      return {
        ...state,
        bookSlotLoading: action.isLoading,
      }
    case 'SET_ACTIVITY_LOADING':
      return {
        ...state,
        allActivityLoading: action.isLoading,
      }
    case 'SET_ACTIVITY_DETAIL_LOADING':
      return {
        ...state,
        activityDetailLoading: action.isLoading,
      }
    default:
      return state
  }
}

export default isLoading
