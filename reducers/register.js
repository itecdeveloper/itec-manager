const initState = {
  page: 1,
  registerDetails: {},
}

const register = (state = initState, action) => {
  switch (action.type) {
    case 'SET_PAGE':
      return {
        ...state,
        page: action.page,
      }
    case 'SET_REGISTER_DETAILS':
      return {
        ...state,
        registerDetails: {
          ...state.registerDetails,
          ...action.details,
        },
      }
    default:
      return state
  }
}

export default register
