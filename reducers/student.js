const initState = {
  allStudents: [],
  allStudentOriginal: [],
  studentDetail: {},
}
const student = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ALL_STUDENT_ORIGINAL':
      return {
        ...state,
        allStudentOriginal: action.allStudentOriginal,
      }
    case 'SET_ALL_STUDENTS':
      return {
        ...state,
        allStudents: action.allStudents,
      }
    case 'SET_STUDENT_DETAIL':
      return {
        ...state,
        studentDetail: action.studentDetail,
      }
    default:
      return state
  }
}

export default student
