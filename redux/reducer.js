import { combineReducers } from 'redux'
import account from '@reducers/account'
import activity from '@reducers/activity'
import register from '@reducers/register'
import modal from '@reducers/modal'
import isLoading from '@reducers/isLoading'
import isAlert from '@reducers/isAlert'
import courses from '@reducers/courses'
import booking from '@reducers/booking'
import student from '@reducers/student'
import orders from '@reducers/orders'
import teacher from '@reducers/teacher'

const rootReducer = combineReducers({
  account,
  activity,
  register,
  modal,
  isAlert,
  isLoading,
  courses,
  booking,
  student,
  orders,
  teacher,
})
export default rootReducer
