const routes = [
  {
    pattern: '/',
    name: 'home',
    page: 'index',
  },
  {
    pattern: '/login',
    name: 'login',
  },
  {
    pattern: '/about',
    name: 'static-about',
  },
]

module.exports = routes
