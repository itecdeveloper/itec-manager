import { Cookies } from 'react-cookie'
import { get } from 'lodash'
import { AUTH_COOKIE_NAME, AUTH_COOKIE_MAX_AGE } from '@config/constants'

export function getCookie(name) {
  if (!process.browser) return false

  const v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)')
  return v ? v[2] : null
}

export function deleteCookie(name, { path = process.env.PATH_COOKIE || '/' } = {}) {
  const d = new Date()
  d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * -1)

  document.cookie = name + '=' + '' + ';path=' + path + ';expires=' + d.toGMTString()
}

export function setCookie(payload) {
  const cookies = new Cookies()
  const expires = get(payload, 'exp') || Math.floor(Date.now() / 1000) + AUTH_COOKIE_MAX_AGE
  cookies.set(AUTH_COOKIE_NAME, payload, {
    path: '/',
    expires: new Date(expires * 1000),
  })
  return getCookies()
}

export function getCookies() {
  const cookies = new Cookies()
  const userData = cookies.get(AUTH_COOKIE_NAME)
  return {
    isAuthenticated: userData ? (userData.token === null ? null : !!userData.token) : !!userData,
    profile: userData ? userData?.detail : '',
    token: userData && userData?.token,
  }
}

export function signOut() {
  deleteCookie(AUTH_COOKIE_NAME)
  location.href = '/login'
}
