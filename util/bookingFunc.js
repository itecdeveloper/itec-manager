import moment from 'moment'

export const bookingType = [
  { label: 'Once', value: 'once' },
  { label: 'Daily', value: 'daily' },
  { label: 'Weekly', value: 'weekly' },
]

export const sessionNumber = (quantity = 0, session = 0) => {
  const range = quantity - session
  let output = []

  for (let i = 0; i < range; i++) {
    const label = i + 1
    const value = i + 1
    output = [...output, { label: label + '', value }]
  }
  return output
}

export const toTimeSteamp = (type, date, time_hour, time_min, session) => {
  let weeks = 0
  let output = []

  if (type === 'daily') {
    for (let i = 0; i < session; i++) {
      const nextDay = moment(date, 'D MMM YYYY').add(i, 'days')
      const dateTimeStr = `${moment(nextDay).format('D MMM YYYY')} ${time_hour}:${time_min}`
      const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
      const timeSteamp = moment(dateTimeFormat).valueOf()
      output = [...output, timeSteamp]
    }
    return output
  } else if (type === 'weekly') {
    for (let i = 0; i < session; i++) {
      const nextWeek = moment(date, 'D MMM YYYY').add(weeks, 'days')
      const dateTimeStr = `${moment(nextWeek).format('D MMM YYYY')} ${time_hour}:${time_min}`
      const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
      const timeSteamp = moment(dateTimeFormat).valueOf()
      output = [...output, timeSteamp]
      weeks = weeks + 7
    }
    return output
  } else {
    const dateTimeStr = `${moment(date).format('D MMM YYYY')} ${time_hour}:${time_min}`
    const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
    const timeSteamp = moment(dateTimeFormat).valueOf()
    return (output = [timeSteamp])
  }
}

const convertHourse2Seccond = (creatTicketTimeLimit) => {
  const hmSplit = creatTicketTimeLimit.split(':')
  const hours = hmSplit[0] === '00' ? 0 : hmSplit[0]
  const minutes = hmSplit[1] === '00' ? 0 : hmSplit[1]
  return parseInt(hours) * 60 * 60 + parseInt(minutes) * 60
}

export const bookAndRescheduleFlow = (sTime, eTime, creatTicketTime, creatTicketTimeLimit) => {
  const sTimeFormat = moment(sTime).format('D MMM YYYY HH:mm')
  const eTimeFormat = moment(eTime).format('D MMM YYYY HH:mm')
  const startTime = moment(sTimeFormat, 'D MMM YYYY HH:mm').valueOf()
  const expiredTime = moment(eTimeFormat, 'D MMM YYYY HH:mm').add(1, 'y').valueOf()
  const today = moment().valueOf()
  const tomorrow = moment().add(86400, 's').valueOf()
  const negativeTime = moment(startTime).subtract(creatTicketTime, 's').valueOf()
  const limitTime = moment().add(convertHourse2Seccond(creatTicketTimeLimit), 's').valueOf()

  if (startTime > expiredTime) {
    return false
  } else {
    return true
  }

  // if (startTime > expiredTime) {
  //   console.log('case 1')
  //   return false
  // } else if (startTime === today) {
  //   console.log('case 2')
  //   return false
  // } else if (negativeTime < today) {
  //   console.log('case 3')
  //   return false
  // } else if (negativeTime > today) {
  //   console.log('case 4')
  //   if (startTime === tomorrow) {
  //     console.log('case 4 - condition 1')
  //     if (limitTime <= today) {
  //       return false
  //     } else {
  //       return true
  //     }
  //   } else {
  //     console.log('case 4 - condition 2')
  //     return true
  //   }
  // }
}

export const dayOfClass = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
]

export const startDate = (day) => {
  let output = []
  let weeks = 7
  const now = moment().day()
  if (now <= day) weeks = 0

  for (let i = 0; i < 15; i++) {
    const dayOfWeek = moment()
      .day(day + weeks)
      .format('D MMM YYYY')
    output = [...output, dayOfWeek]
    weeks = weeks + 7
  }
  return output
}
