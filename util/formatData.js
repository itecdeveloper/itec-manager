import {
  filterStudentById,
  filterOrderDetailById,
  filterSessionStatusActivate,
  filterAccount,
  filterCourseId,
  filterSessionCourseId,
} from '@util/filterData'
import { toDateTimeFormat } from '@util/formatDate'
import { calcAge } from '@util/orderFunc'

export const formatDataOrders = (items, accounts) => {
  const results = []
  if (Object.keys(items).length > 0) {
    items.map((item) => {
      const account = filterStudentById(accounts, item.DETAIL.STUDENT_ID)
      let body = {
        ...item.DETAIL,
        FISRT_NAME: account.length > 0 ? account[0].FISRT_NAME : '-',
        LAST_NAME: account.length > 0 ? account[0].LAST_NAME : '-',
        age: calcAge(account.length > 0 ? account[0].DOB : 0),
        CONTACT_NO: account.length > 0 ? account[0].CONTACT_NO : '-',
        SESSIONS: filterSessionStatusActivate(item.SESSIONS),
        STUDENT: `${account.length > 0 ? account[0].FISRT_NAME : '-'} ${
          account.length > 0 ? account[0].LAST_NAME : '-'
        }`,
      }
      results.push(body)
    })
  }
  return results
}
export const formatDataActivity = (data) => {
  const result = []
  if (Object.keys(data).length > 0) {
    data.Items.map((item) => {
      let ACTIVITY_TYPE = '-'
      const ACCOUNT = filterAccount(data.ACCOUNTS, item.BY)
      const NAME = ACCOUNT.length ? `${ACCOUNT[0].FISRT_NAME} ${ACCOUNT[0].LAST_NAME}` : item.BY
      const ACCOUNT_TYPE = ACCOUNT.length ? ACCOUNT[0].TYPE : item.BY
      const ORDER_ID = item.NEW.ORDER_ID
      if (item.TYPE === 'ORDER') {
        if (item.OLD.NEW && item.OLD.NEW === 'NEW') {
          ACTIVITY_TYPE = 'Create Order'
        } else if (
          (item.OLD.STATUS === 'ORDER_CREATE' || item.OLD.STATUS === 'REJECT') &&
          item.NEW.STATUS === 'CANCEL'
        ) {
          ACTIVITY_TYPE = 'Cancel Order'
        } else if (item.OLD.STATUS === 'ORDER_CREATE' && item.OLD.STATUS === 'REJECT') {
          ACTIVITY_TYPE = 'Reject Order'
        } else {
          ACTIVITY_TYPE = 'Edit Order'
        }
      }

      if (item.TYPE === 'SESSION') {
        if (item.DETAIL === 'req_rescedule_session') {
          ACTIVITY_TYPE = 'Request Reschedule'
        } else if (item.DETAIL === 'approve_rescedule_session') {
          ACTIVITY_TYPE = 'Approve Reschedule'
        } else if (item.DETAIL === 'cancel_rescedule_session') {
          ACTIVITY_TYPE = 'Cancel Reschedule' // 0
        } else if (item.DETAIL === 'reject_rescedule_session') {
          ACTIVITY_TYPE = 'Deny Reschedule'
        } else if (item.OLD.S_STATUS === 'WAITING_APPROVE' && item.NEW.S_STATUS === 'INACTIVE') {
          ACTIVITY_TYPE = 'Deny Session'
        } else if (item.OLD.S_STATUS === 'WAITING_APPROVE' && item.NEW.S_STATUS === 'ACTIVE') {
          ACTIVITY_TYPE = 'Approve Session'
        } else if (item.OLD.S_STATUS === 'ACTIVE' && item.NEW.S_STATUS === 'COMPLETE') {
          ACTIVITY_TYPE = 'Complete Session'
        } else if (item.OLD.S_STATUS === 'COMPLETE' && item.NEW.S_STATUS === 'ACTIVE') {
          ACTIVITY_TYPE = 'Incomplete Session'
        } else if (item.OLD.NEW && item.OLD.NEW === 'NEW') {
          ACTIVITY_TYPE = 'Create Session'
        } else {
          ACTIVITY_TYPE = 'Edit Session'
        }
      }
      result.push({
        ...item,
        ACTIVITY_TYPE,
        ORDER_ID,
        NAME,
        ACCOUNT_TYPE,
      })
    })
  }

  return result
}
export const formatDataSessionByTeacher = (items) => {
  const result = []
  if (Object.keys(items).length > 0) {
    items.SESSIONS.map((item) => {
      if (item.STATUS_NEW_START_TIME === 'req') {
        const account = filterStudentById(items.ACCOUNTS, item.STUDENT_ID)
        const detail = filterOrderDetailById(items.DETAIL, item.ORDER_ID)
        result.push({
          orderId: item.ORDER_ID,
          sessionId: item.SESSION_ID,
          status: item.S_STATUS,
          studentId: item.STUDENT_ID,
          studentName:
            account.length > 0 ? `${account[0].FISRT_NAME} ${account[0].LAST_NAME}` : '-',
          studentContact: account.length > 0 ? account[0].CONTACT_NO : '-',
          dateTime: item.START_TIME,
          newDateTime: item.NEW_START_TIME,
          courseId: detail.length > 0 ? detail[0].COURSE_ID : '-',
          levelId: detail.length > 0 ? detail[0].LEVEL_ID : '-',
        })
      }
    })
  }
  return result
}
export const compareValues = (key, order = 'asc') => {
  return function (a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) return 0

    const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key]
    const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key]

    let comparison = 0
    if (varA > varB) comparison = 1
    else if (varA < varB) comparison = -1

    return order == 'desc' ? comparison * -1 : comparison
  }
}
export const formatMyCourse = (orders, courses) => {
  const sessions = orders.SESSIONS
  const structures = orders.DETAIL
  const formatStructure = structures.reduce((arr, structure) => {
    const itemCourse = filterCourseId(courses, structure.COURSE_ID)
    const itemSessions = filterSessionCourseId(sessions, structure.ORDER_ID)
    const reducer = {
      ...structure,
      COURSE_DETAIL: itemCourse.length > 0 ? itemCourse[0] : {},
      SESSIONS: itemSessions.length > 0 ? itemSessions : [],
    }
    if (structure.STATUS === 'PAID') {
      arr['orderSuccess'] = [...(arr['orderSuccess'] || []), reducer]
    } else {
      arr['orderNotSuccess'] = [...(arr['orderNotSuccess'] || []), reducer]
    }
    return arr
  }, {})
  return formatStructure
}
export const formatCalendar = async (data) => {
  let structures = []
  await data.map((item) => {
    structures = [
      ...structures,
      {
        ...item,
        START_TIME_FORMATE: toDateTimeFormat(item.START_TIME),
        END_TIME_FORMATE: toDateTimeFormat(item.END_TIME),
      },
    ]
  })
  const formatStructure = structures.reduce((arr, structure) => {
    arr[structure.START_TIME_FORMATE.date] = [
      ...(arr[structure.START_TIME_FORMATE.date] || []),
      structure,
    ]
    return arr
  }, {})
  return formatStructure
}
export const formatCalendarCourses = async (courses) => {
  let structures = []
  await courses.map((course) => {
    course.SESSIONS.map((session) => {
      if (session.S_STATUS === 'ACTIVE' || session.S_STATUS === 'COMPLETE') {
        structures = [
          ...structures,
          {
            ...session,
            START_TIME_FORMATE: toDateTimeFormat(session.START_TIME),
            END_TIME_FORMATE: toDateTimeFormat(session.END_TIME),
            MY_COUSE: course,
            SHOW_TYPE: 'COURSE',
          },
        ]
      }
    })
  })

  const formatStructure = structures.reduce((arr, structure) => {
    arr[structure.START_TIME_FORMATE.date] = [
      ...(arr[structure.START_TIME_FORMATE.date] || []),
      structure,
    ]
    return arr
  }, {})
  return formatStructure
}
export const mergeFormatCalendar = async (items1, items2) => {
  let results = []
  Object.keys(items1).map((date1) => {
    results[date1] = items1[date1]
    Object.keys(items2).map((date2) => {
      if (date1 === date2) {
        results[date1] = [...items1[date1], ...items2[date1]]
      } else {
        results[date2] = items2[date2]
      }
    })
  })
  return results
}
