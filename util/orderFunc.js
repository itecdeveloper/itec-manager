import moment from 'moment'

export const calcAge = (date) => {
  const nowAge = moment().diff(moment(date), 'years')
  return nowAge
}

export const showOrderStatus = (status) => {
  // ORDER_CREATE, WAITING, PAID
  switch (status) {
    case 'ORDER_CREATE':
      return 'Pending'
    case 'WAITING':
      return 'Pending'
    case 'PAID':
      return 'Successful'
    case 'CANCEL':
      return 'Cancelled'
    case 'REJECT':
      return 'Rejected'
    case 'INACTIVE':
      return 'Cancelled'
    default:
      return '-'
  }
}

export const showOrderPaymentType = (type) => {
  switch (type) {
    case '2c2p':
      return '2C2P: Credit/Cash'
    case 'bank_tran':
      return 'Bank Transfer'
    default:
      return '-'
  }
}

export const showStatusReschedule = (status) => {
  switch (status) {
    case 'req':
      return 'Pending'
    case 'reject':
      return 'Denied'
    case 'approve':
      return 'Approved'
    case 'cancel':
      return 'Cancelled'
    default:
      return '-'
  }
}

export const showStatusSession = (status) => {
  switch (status) {
    case 'WAITING_APPROVE':
      return 'Pending'
    case 'ACTIVE':
      return 'Incomplete'
    case 'INACTIVE':
      return 'Denied'
    case 'COMPLETE':
      return 'Completed'
    default:
      return '-'
  }
}

export const showStatusSessionApprove = (status) => {
  switch (status) {
    case 'WAITING_APPROVE':
      return 'Pending'
    case 'ACTIVE':
      return 'Approved'
    case 'INACTIVE':
      return 'Denied'
    case 'COMPLETE':
      return 'Completed'
    default:
      return '-'
  }
}

export const paymentStatusOptions = () => {
  const options = [
    { value: 'ORDER_CREATE', label: 'Waiting for Payment' },
    { value: 'PAID', label: 'Successful' },
  ]
  return options
}

export const getLevelByCourse = (items, userAge) => {
  let results = []
  if (Object.keys(items).length > 0) {
    items.LEVEL.map((item) => {
      if (item.LEVEL !== 'DETAIL') {
        let ageSplit = item.AGE.split('-')
        if (userAge >= ageSplit[0] && userAge <= ageSplit[1]) {
          let body = {
            value: item.LEVEL_ID,
            label: item.LEVEL,
            teachers: getTeacherByCourse(items.ACCOUNTS, item.TEACHERS),
          }
          results.push(body)
        }
      }
    })
  }
  return results
}

export const getTeacherByCourse = (accounts, teachers) => {
  let results = []
  if (Object.keys(teachers).length > 0) {
    teachers.map((teacher) => {
      let detail = accounts.filter((account) => account.ACCOUNT_ID === teacher)
      if (detail.length > 0) {
        results.push({
          value: teacher,
          label: `${detail[0].FISRT_NAME} ${detail[0].LAST_NAME}`,
        })
      }
    })
  }
  return results
}

export const setGroupOrders = (items) => {
  let results = {}
  let pendingOrders = []
  let orders = []
  if (Object.keys(items).length > 0) {
    items.map((item) => {
      if (
        item.LEVEL_ID === '-' ||
        item.LEVEL === '-' ||
        item.TEACHER_ID === '-' ||
        item.TEACHER === '-' ||
        item.STATUS !== 'PAID' ||
        filterSessionReschedule(item.SESSIONS, 'req').length > 0 ||
        filterSessionStatus(item.SESSIONS, 'WAITING_APPROVE').length > 0
      ) {
        pendingOrders.push(item)
      } else {
        orders.push(item)
      }
    })
  }
  results = {
    pendingOrders,
    orders,
  }
  return results
}

export const filterSessionReschedule = (data, type) => {
  const filterFunc = (data) => {
    return data.STATUS_NEW_START_TIME === type
  }
  return data.filter(filterFunc)
}

export const filterSessionStatus = (data, type) => {
  const filterFunc = (data) => {
    return data.S_STATUS === type
  }
  return data.filter(filterFunc)
}

export const mapCourseData = (courseId, levelId, teacherId, items) => {
  const course = items.find((item) => item.DETAIL.COURSE_ID === courseId)
  const level = filterLevelById(course.LEVEL || [], levelId)
  const teacher = filterTeacherById(course.ACCOUNTS || [], teacherId)
  const results = {
    COURSE_NAME: course.DETAIL.COURSE_NAME,
    CREATE_TICKET_TIME: course.DETAIL.CREATE_TICKET_TIME,
    CREATE_TICKET_TIME_LIMIT: course.DETAIL.CREATE_TICKET_TIME_LIMIT,
    LEVEL: level.length > 0 ? level[0].LEVEL : '-',
    TEACHER: teacher.length > 0 ? `${teacher[0].FISRT_NAME} ${teacher[0].LAST_NAME}` : '-',
  }
  return course ? results : {}
}

export const filterLevelById = (data, id) => {
  const filterFunc = (data) => {
    return data.LEVEL_ID === id
  }
  return data.filter(filterFunc)
}

export const filterTeacherById = (data, id) => {
  const filterFunc = (data) => {
    return data.ACCOUNT_ID === id
  }
  return data.filter(filterFunc)
}
